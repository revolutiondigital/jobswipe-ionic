angular.module('starter.directives',[])
//**********************************************
//Directive used to display match
    .directive('matchMade',function(){
    return{
        restrict:"E",
        templateUrl:"templates/match-made.html",
        controller:'matchCtrl',
        scope:{data : '='}
   }
    })
//******************************************************************
//DIRECTIVE used to display additional information
    .directive('additionalInformation',function(){
    return{
        restrict:"E",
        templateUrl:"templates/additional-information.html",
        controller:'additionalInformationCtrl',
        scope:{data : '='}
   }
    })
