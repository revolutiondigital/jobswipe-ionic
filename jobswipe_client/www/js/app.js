// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','fiestah.money', 'ionic.contrib.ui.tinderCards','cropme','starter.directives','ngImgCrop','angularPayments'])
 .filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}])
.filter('array', function() {
    return function(arrayLength) {
        if (arrayLength) {
            arrayLength = Math.ceil(arrayLength);
            var arr = new Array(arrayLength), i = 0;
            for (; i < arrayLength; i++) {
                arr[i] = i;
            }
            return arr;
        }
    };
})
.run(function($rootScope, $ionicPlatform, $ionicSideMenuDelegate) {
  $rootScope.$on('$stateChangeSuccess', function () {
      // By default, we want to allow the side-menu to be dragged. Some views may need to disable it
          // so we want to enable it after a successful state change
              $ionicSideMenuDelegate.canDragContent(false);
                });
                })
.directive('noDragRight', ['$ionicGesture', function($ionicGesture,$ionicSideMenuDelegate) {

  return {
      restrict: 'A',
      link: function($scope, $element, $attr) {
	  console.log('he;')
	        $ionicGesture.on('dragright', function(e) {
		            e.gesture.srcEvent.preventDefault();
			          }, $element);
				      }
				        }
}])
.run(function($ionicPlatform,$ionicSideMenuDelegate, $ionicNavBarDelegate,$rootScope,$state,$ionicHistory,newNotificationService,RootSettings,$location,$window) {

    //make basck exit app if they are at home

     $ionicPlatform.registerBackButtonAction(function (event) {
           if($state.current.name==="employer.employerDashboard" || $state.current.name === "employee.searchJobs"){
                 navigator.app.exitApp();
                     }
         else {
	    $ionicHistory.goBack()
                                   }

                                     }, 100);
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
//toggle left side menu
 $rootScope.toggleLeftSideMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
  //toggle right side menu
   $rootScope.toggleRightSideMenu = function() {
    $ionicSideMenuDelegate.toggleRight();
  };
  //toggle wipe right
 $rootScope.onSwipeRight = function() {
 $ionicSideMenuDelegate.hideLeft();
}
 $rootScope.onSwipeLeft = function() {
 $ionicSideMenuDelegate.hideRight();
}
//Go Back
$rootScope.goBack = function(){
    console.log($state.current.name)
   if($state.current.name==="employer.employerDashboard" || $state.current.name === "employee.searchJobs"){
	       return
	   }

    $ionicHistory.goBack()
            }

 $rootScope.editProfilePicture = function() {
     $state.go('changepicture.changeProfilePicture')
}


  //degree to rad function
  $rootScope.deg2rad = function(deg){
		return deg * (Math.PI/180)
		}

  //function used to go to employer Profile 
   $rootScope.getEmployerProfile= function(id){
	$state.go('employer.viewProfile', {id:id})
       }
 //FUNCTION GOTO JOB details
    $rootScope.getJob = function(id){
	$state.go('jobs.getJob',{id:id})
	}

 //FUNCTION GOTO JOB details
    $rootScope.getJob = function(id){
	$state.go('jobs.getJob',{id:id})
	}


  // Function used to go to messages dashboard
   $rootScope.openMessages = function(){
       newNotificationService.setNewMessages(false)
       $rootScope.$emit('refreshMessages')
       $state.go('messages.messagesDashboard')
       }
       //FUNCTION USED TO HANDLE LOGOUT
   $rootScope.userLogout = function(){
       QB.logout()
   openFB.logout(function() {
       RootSettings = {}
	$location.path('/') 
	$window.location.reload()
   })
   }
})
//LOADING value
.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        $rootScope.$broadcast('loading:show')
        return config
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide')
        return response
      }
    }
  })
})
.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
     $httpProvider.defaults.useXDomain = true;
   }])
// .run(function($rootScope, $ionicLoading) {
//   $rootScope.$on('loading:show', function() {
//     $ionicLoading.show({template: "<i class='icon ion-looping default-loading'></i>"})
//   })

//   $rootScope.$on('loading:hide', function() {
//     $ionicLoading.hide()
//   })
// })

.config(function($stateProvider, $urlRouterProvider,$httpProvider) {
   openFB.init({appId:'797804093619145'});
  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })
    //state for startup
    .state('app.startup', {
      url: "/startup",
      views: {
        'menuContent' :{
          templateUrl: "templates/startup.html",
          controller: 'authenticationCtrl'
        }
      }
    })

    //STATE FOR SIGNUP 
    .state('app.loginPage', {
      url: "/loginpage",
      views: {
        'menuContent' :{
          templateUrl: "templates/authentication/login-page.html",
          controller: 'authenticationCtrl'
        }
      }
    })
 .state('app.employeeIntro', {
      url: "/employeeintro",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/intro.html",
	  controller:"introCtrl"
        }
      }
    })
 .state('app.employerIntro', {
      url: "/employerintro",
      views: {
        'menuContent' :{
          templateUrl: "templates/employer/intro.html",
	  controller:"introCtrl"
        }
      }
    })

    //Choose whether user if employee or employer
    .state('app.chooseUserType', {
      url: "/chooseusertype",
      views: {
        'menuContent' :{
          templateUrl: "templates/userinfo/choose-user-type.html",
          controller: 'userSignupCtrl'
        }
      }
    })

    //Choose whether user if employee or employer
    .state('app.employeeSignup', {
      url: "/employeesignup",
      views: {
        'menuContent' :{
          templateUrl: "templates/userinfo/employee-signup.html",
          controller: 'userSignupCtrl'
        }
      }
    })

    //Choose whether user if employee or employer
    .state('app.employerSignup', {
      url: "/employersignup",
      views: {
        'menuContent' :{
          templateUrl: "templates/userinfo/employer-signup.html",
          controller: 'userSignupCtrl'
        }
      }
    })
      .state('changepicture', {
      url: "/changepicture",
      abstract: true,
      templateUrl: "templates/change-picture/menu.html",
      controller: 'changeUserDataCtrl'
    })
    //Choose whether user if employee or employer
    //state to change profile pic
    .state('changepicture.changeProfilePicture', {
      url: "/changeprofilepicture",
      views: {
        'menuContent' :{
          templateUrl: "templates/userinfo/change-profile-picture.html",
          controller: 'changeUserDataCtrl'
        }
      }
    })
    .state('changepicture.choosePicture', {
      url: "/choosepicture/:type",
      views: {
        'menuContent' :{
          templateUrl: "templates/userinfo/picture-list.html",
          controller: 'pictureSelectorCtrl',
          params:['displayPictureChoices']


        }
      }
    })
    .state('changepicture.pictureCrop', {
      url: "/croppicture/:id",
      views: {
        'menuContent' :{
          templateUrl: "templates/userinfo/picture-crop.html",
          controller: 'pictureCropCtrl',
          params:['id']


        }
      }
    })

    .state('employer', {
      url: "/employer",
     abstract: true,
      templateUrl: "templates/employer/menu.html",
      controller: 'employerMenuCtrl'
    })
      

    //EMPLOYER STATES 

    .state('employer.editProfile', {
      url: "/editprofile",
      views: {
        'menuContent' :{
          templateUrl: "templates/employer/edit-profile.html",
          controller: 'employerProfileCtrl',
        }
      }
    })
 .state('employer.viewProfile', {
      url: "/viewprofile/:id",
      views: {
        'menuContent' :{
          templateUrl: "templates/employer/view-profile.html",
          controller: 'employerProfileCtrl',
          params:['id']
        }
      }
    })

    .state('employer.createJob', {
      url: "/createjob",
      views: {
        'menuContent' :{
          templateUrl: "templates/jobs/create-job.html",
          controller: 'createJobCtrl',
        }
      }
    })
    //get users that have swiped right to job
    .state('employer.getJobEmployees', {
      url: "/employergetjobemployees/:id",
      views: {
        'menuContent' :{
          templateUrl: "templates/jobs/employer-get-job-employees.html",
          controller: 'employerSwipeCtrl',
          params:['id']
          
        }
      }
    })
    .state('employer.createJobSuccess', {
      url: "/createjobsuccess",
      views: {
        'menuContent' :{
          templateUrl: "templates/jobs/create-job-success.html",
          controller: 'createJobCtrl',
        }
      }
    })
    .state('employer.getJobMatches', {
      url: "/getjobmatches",
      views: {
        'menuContent' :{
          templateUrl: "templates/jobs/get-job-matches.html",
          controller: 'jobMatchCtrl',
        }
      }
    })

    .state('employer.employerDashboard', {
      //id is for recently created job to show user
      url: "/employerdashboard/:id",
      views: {
        'menuContent' :{
          templateUrl: "templates/employer/dashboard.html",
          controller: 'employerDashboardCtrl',
          params:['id']
        }
      }
    })
    //EMPLOYEE STATES
   .state('employee', {
      url: "/employee",
      abstract: true,
      templateUrl: "templates/employee/menu.html",
      controller: 'employeeMenuCtrl'
    })
   .state('employee.searchJobs', {
      url: "/searchjobs",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/search-jobs.html",
          controller: 'employeeSwipeCtrl',
        }
      }
    })
 .state('employee.getEmail', {
      url: "/getEmail",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/get-email.html",
          controller: 'employeeGetEmailCtrl',
        }
      }
    })
.state('employee.getJobMatches', {
      url: "/getjobmatches",
      views: {
        'menuContent' :{
          templateUrl: "templates/jobs/get-job-matches.html",
          controller: 'jobMatchCtrl',
        }
      }
    })

 .state('employee.viewProfile', {
      url: "/viewprofile/:id",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/view-profile.html",
          controller: 'employeeProfileCtrl',
          params:['id']
        }
      }
    })
 .state('employee.updateAddress', {
      url: "/updateaddress",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/update-address.html",
          controller: 'employeeProfileCtrl',
        }
      }
    })
.state('employee.editProfile', {
      url: "/editprofile",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/edit-profile.html",
          controller: 'editEmployeeProfileCtrl',
        }
      }
    })
.state('employee.viewSettings', {
      url: "/viewsettings",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/view-settings.html",
          controller: 'employeeMenuCtrl',
        }
      }
    })



.state('employee.changeDiscoveryPreferences', {
      url: "/changediscoverypreferences",
      views: {
        'menuContent' :{
          templateUrl: "templates/employee/change-discovery-preferences.html",
          controller: 'changeDiscoveryPreferencesCtrl',
        }
      }
    })

//MESSAGES STATE
   .state('messages', {
      url: "/messages",
      abstract: true,
      templateUrl: "templates/messages/menu.html",
      controller: 'messagesCtrl'
    })
 
 .state('messages.messagesDashboard', {
      url: "/messagesdashboard",
    views: {
        'menuContent' :{
      templateUrl: "templates/messages/dashboard.html",
      controller: 'messagesCtrl'
      }
      }
    })
  .state('messages.chatRoom', {
      //id is for recently created job to show user
      url: "/chatroom/:id",
      views: {
        'menuContent' :{
          templateUrl: "templates/messages/chatroom.html",
          controller: 'chatroomCtrl',
          params:['id']
        }
      }
    })

  //state for jobs
  .state('jobs', {
      url: "/jobs",
      abstract: true,
      templateUrl: "templates/jobs/menu.html",
      controller: 'jobsCtrl'
    })
// get job stuff 
 .state('jobs.getJob', {
      url: "/getJob/:id",
    views: {
        'menuContent' :{
      templateUrl: "templates/jobs/view-job.html",
      controller: 'jobProfileCtrl'
      }
      }
    })

// get job stuff 
 .state('jobs.editJob', {
      url: "/editJob/:id",
    views: {
        'menuContent' :{
      templateUrl: "templates/jobs/edit-job.html",
      controller: 'editJobCtrl'
      }
      }
    })


    .state('app.search', {
      url: "/search",
      views: {
        'menuContent' :{
          templateUrl: "templates/search.html"
        }
      }
    })

    .state('app.browse', {
      url: "/browse",
      views: {
        'menuContent' :{
          templateUrl: "templates/browse.html"
        }
      }
    })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent' :{
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.single', {
      url: "/playlists/:playlistId",
      views: {
        'menuContent' :{
          templateUrl: "templates/playlist.html",
          controller: 'PlaylistCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
   $urlRouterProvider.otherwise('/app/startup');
})
//FACEBOOK Login
.config(["$locationProvider", function($locationProvider) {
         $locationProvider.hashPrefix('!');
             }])
 .config(function() {
     // Set your appId through the setAppId method or
     // use the shortcut in the initialize method directly.
  })


// //You can use the isReady function to get notified when the Facebook SDK is ready
// $.ajaxSetup({ 
//      beforeSend: function(xhr, settings) {
//          function getCookie(name) {
//              var cookieValue = null;
//              if (document.cookie && document.cookie != '') {
//                  var cookies = document.cookie.split(';');
//                  for (var i = 0; i < cookies.length; i++) {
//                      var cookie = jQuery.trim(cookies[i]);
//                      // Does this cookie string begin with the name we want?
//                  if (cookie.substring(0, name.length + 1) == (name + '=')) {
//                      cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
//                      break;
//                  }
//              }
//          }
//          return cookieValue;
//          }
//          if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
//              // Only send the token to relative URLs i.e. locally.
//              xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
//          }
//      } 
// });
