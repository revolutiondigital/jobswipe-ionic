angular.module('starter.controllers', ['ionic.contrib.ui.tinderCards'])

.factory('RootSettings', function() {
  return {
     // url : 'http://192.168.2.129:8000',
      url : 'http://jobswipeionicserver-env-iempibbzdt.elasticbeanstalk.com',

      accessToken : "",
      userDetails:{},
      currentLatitude :"",
      currentLongitude : ""
  };
}) 
//SERVICE USED TO GET LOCATION
.factory('locationService', function($http, RootSettings, $q) {
        return {//GET current latitude and longitude of user
            getLocation: function() {
             
              var promise = navigator.geolocation.getCurrentPosition(function(position){
                RootSettings.currentLongitude=position.coords.longitude
                RootSettings.currentLatitude=position.coords.latitude
                return_data = {'latitude':position.coords.latitude, 'longitude':position.coords.longitude}
                console.log(return_data)
                return(return_data)
              },function(data){alert("We couldn't get your location")},{ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })

               return promise
            }
           
        }

      })           


//Service used to translate match details between controllers
.factory('matchDataFactory',function(){
    return{
        matchData:{}
        }
    })

//Service used to translate profile changes details between controllers
.factory('profileDataFactory',function(){
    return{
        profile:{}
        }
    })

//Service used to translate match details between controllers
.factory('additionalInformationDataFactory',function(){
    return{
        additionalInformation:{}
        }
    })
//Service used to collect jobs that users have been accepted for
.factory('chatroomJobDataFactory',function(){
    return{
        chatroomJobs:[],
        otherUserID:null,
        }
    })


//SERVICE USED TO GET PICTURES FROM FACEBOOK
.factory('pictureService', function($http, RootSettings, $q) {
        return {
            getFBProfiles: function(after_id) {
              if (after_id === null){
                var afterID = ""
              }else{
                var afterID = after_id
              }
              var returnProfilePictures=[]
              // get 100 of the next photos
                return $http.get(
                    "https://graph.facebook.com/v2.2//me/photos?access_token=" + RootSettings.userDetails.accessToken+"&after=" + afterID
                    ).then(function (response) {

                      for(i = 0 ; i < response.data.data.length ; i++){
                        profileUrl ={}
                        profileUrl.id = response.data.data[i].id
                        //get the thumbnail
                        for( z = 0 ;z < response.data.data[i].images.length ; z++){
                          if(response.data.data[i].images[z].height === 960){
                            profileUrl.thumbnail = response.data.data[i].images[z].source
                          }
                        }
                        returnProfilePictures.push(profileUrl)
                      }
                      afterID = response.data.paging.cursors.after
                      return_data ={}
                      console.log(returnProfilePictures)
                      return_data.afterID = afterID
                      return_data.returnProfilePictures = returnProfilePictures
                      return return_data
                    })
            },
            getFBPicture:function(picture_id){
              return $http.get(
                    "https://graph.facebook.com/v2.2/" + picture_id+"?access_token=" + RootSettings.userDetails.accessToken
                    ).then(function (response) {
                      console.log(response)
                     
                         for( z = 0 ;z < response.data.images.length ; z++){
                          if(response.data.images[z].height === 600){
                            return response.data.images[z].source
                          }
                        }
                      
                      })
            }        
        };
    })
//SERVICE USED TO GET NEW MESSAGES
.factory('newMessageFactory',function(){
    return{
        newMessage:{}
        }
    })


//SERVICE USED TO GET Store messages 
.factory('matchMessagesFactory',function(){
    return{
        matchMessages:[]
        }
    })

//SERVICE USED TO Delete Jobs 
.factory('deleteJobService', function($http, RootSettings, $q, $rootScope,$state) {
        return {
            deleteJob: function(job_id) {
		post_data= {}
                post_data.userID= RootSettings.userDetails.userID
                post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.jobID= job_id
                $http.post(RootSettings.url + '/jobs/deletejob',post_data).success(function(data){
		    $rootScope.$emit('deleteJob',{job_id:job_id})
		    $state.go('employer.employerDashboard')
		})

                 
	    },
	}  
})


//SERVICE USED TO CALCULATE DISTANCE BETWEEN 2 LAT/LONG POINTS
.factory('distanceService', function($http, RootSettings, $q, $rootScope) {
        return {
	    //return string off distance between 2 latitude and longitude points
            getDistance: function(lat1, lon1, lat2,lon2) {
		console.log(lat1 +" "+ lon1)
		console.log(lat2 +" "+ lon2)
		var R = 6371; // Radius of the earth in km
		var dLat = $rootScope.deg2rad(lat2-lat1);  // deg2rad below
		var dLon = $rootScope.deg2rad(lon2-lon1); 
	        var a = 
		     Math.sin(dLat/2) * Math.sin(dLat/2) +
		     Math.cos($rootScope.deg2rad(lat1)) * Math.cos($rootScope.deg2rad(lat2)) * 
		    Math.sin(dLon/2) * Math.sin(dLon/2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c; // Distance in km
		if(d <1 ){
		    d = "less then one"
		 }else{
		     d = d.toString()

         d =d.split('.')
         d = d[0]
		     }
		console.log(d)
		return d;
	    },
		}
})
 
//SERVICE USED TO HANDLE ALL NEW NOTIFICATION FUNCTIONALITY
.factory('newNotificationService', function($http, RootSettings, $q, $rootScope) {
        return {
	    //Check to see if there are new matches
            checkNewMatches: function() {
		params = {}
		params['userID'] = RootSettings.userDetails.userID
		params['accessToken']= RootSettings.userDetails.accessToken
		params['notificationType'] ='matches'
		params['userType'] =RootSettings.userDetails.userType
		$http.get(RootSettings.url + '/userinfo/getnewnotifications',{params:params}).success(function(data){
		if(data === "true"){
      if(RootSettings.userDetails.userType === "employee"){
		      $rootScope.showNewMatchNotificationEmployee = true
        }else if (RootSettings.userDetails.userType === "employer"){
          $rootScope.showNewMatchNotificationEmployer = true

        }
		   }else if (data ==="false"){
        if(RootSettings.userDetails.userType === "employee"){
          $rootScope.showNewMatchNotificationEmployee = false
        }else if (RootSettings.userDetails.userType === "employer"){
          $rootScope.showNewMatchNotificationEmployer = false

        }
		   }
		})
		

	    },
	    //check to see if there are any new messsages to dispalay notification
	    checkNewMessages:function() {
	    	params = {}
		params['userID'] = RootSettings.userDetails.userID
		params['accessToken']= RootSettings.userDetails.accessToken
		params['notificationType'] ='messages'
		params['userType'] =RootSettings.userDetails.userType
		$http.get(RootSettings.url + '/userinfo/getnewnotifications',{params:params}).success(function(data){
		   console.log(data) 
       if(RootSettings.userDetails.userType === "employee"){
       	if (data ==="false"){
       			$rootScope.showNewMessageNotificationEmployee = false
       	}else{
       		$rootScope.showNewMessageNotificationEmployee = true
       	}


       }else if (RootSettings.userDetails.userType === "employer"){
       		if (data ==="false"){
       			$rootScope.showNewMessageNotificationEmployer = false
       	}else{
       		$rootScope.showNewMessageNotificationEmployer = true
       	}
       }
		   if(data === "true"){
		       //check to see if previous message was false. If it was,
			// now it is true, so we need to update matches for user 
		if($rootScope.hasNewMessage != "undefined"){
			if($rootScope.hasNewMessage === "false"){
			    $rootScope.$emit("updateMatches")
			}
		       }
		   }

		$rootScope.hasNewMessage = data
		})
		

	    },
	    setNewMatches:function(val) {
		post_data = {}
		post_data['userID'] = RootSettings.userDetails.userID
		post_data['accessToken']= RootSettings.userDetails.accessToken
		post_data['notificationType'] ='matches'
		post_data['value'] = val.toString()
		post_data['userType'] =RootSettings.userDetails.userType
    $http.post(RootSettings.url + '/userinfo/updatenewnotifications',post_data).success(function(data){
       if(RootSettings.userDetails.userType === "employee"){
       $rootScope.showNewMatchNotificationEmployee = val

       }else if (RootSettings.userDetails.userType === "employer"){
       $rootScope.showNewMatchNotificationEmployer = val

       }

		})
	    	
	    },
	    setNewMessages:function(val) {
		//if there is a new message we need to update messagelist
		if(val === true){
		   $rootScope.$emit('updateMatches')
		}
		post_data = {}
		post_data['userID'] = RootSettings.userDetails.userID
		post_data['accessToken']= RootSettings.userDetails.accessToken
		post_data['notificationType'] ='messages'
		post_data['value'] = val.toString()
		post_data['userType'] =RootSettings.userDetails.userType
                $http.post(RootSettings.url + '/userinfo/updatenewnotifications',post_data).success(function(data){
		   if (RootSettings.userDetails.userType === "employee"){
           $rootScope.showNewMessageNotificationEmployee = val
       }else if(RootSettings.userDetails.userType === "employer") {
           $rootScope.showNewMessageNotificationEmployer = val

       }

		})

	    }
		}
})
//SERVICE USED TO GET LOCATION
.factory('quickbloxService', function($http, RootSettings, $q, newMessageFactory,$rootScope,matchMessagesFactory,newNotificationService) {
        return {//GET current latitude and longitude of user
            connectToQB: function() {
             $http({url:RootSettings.url + '/userinfo/getquickbloxsession',
          method:"GET",
      }).then(function(data){
	  console.log(data)
	  QB.init(data.data.session.token)
	  RootSettings.QBToken = data.data.session.token
	  console.log(RootSettings.QBToken)
	  
	  params = {login:RootSettings.userDetails.userID, password:RootSettings.userDetails.userID}
	  var chatUser={}
	  console.log(params)
	  //try logging in to QB
	QB.login(params,function(res,err){
	    try{
	    chatUser.jid = err.id.toString()+"-18291@chat.quickblox.com"
	    chatUser.password = params.password
	    console.log(chatUser)
	QB.chat.connect(chatUser,function(err,roster){
	    //on message listener that performs callback when we get a msesage
	    QB.chat.onMessageListener = function(userID,message){
		newMessageFactory.newMessage.message = message 
		newMessageFactory.newMessage.userID = userID 
		//we need to show new messages
		console.log(userID)
		
		//we need to update the message lists
		for (var i = 0, l = matchMessagesFactory.matchMessages.length; i < l; i ++) {
		    if(matchMessagesFactory.matchMessages[i].dialogID === message.extension.dialog_id){
			//change last message sent
			matchMessagesFactory.matchMessages[i].showMessage = message.body
			matchMessagesFactory.matchMessages[i].isUnread = true
			match_message = matchMessagesFactory.matchMessages[i]
			//remove message from array and readd to top
			matchMessagesFactory.matchMessages.splice(i,1)
			matchMessagesFactory.matchMessages.unshift(match_message)
			}
		}
		$rootScope.$emit('newMessage')
		}
	    })

	    }
	    //if login fails we need to sign up
	    catch(err){
		params.full_name = RootSettings.userDetails.first_name + " " + RootSettings.userDetails.last_name
		QB.users.create(params, function(err,result){
		    console.log(result)

		chatUser.jid = result.id.toString()+"-18291@chat.quickblox.com"
		post_data= {}
                post_data.socialUserID = RootSettings.userDetails.userID
                post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.QBID= result.id
    		$http.post(RootSettings.url + '/userinfo/setquickbloxid',post_data).success(function(data){
		    console.log(data)
		    RootSettings.userDetails.quickbloxID = data.data
		 })

	QB.login(params,function(res,err){
	    chatUser.jid = err.id.toString()+"-18291@chat.quickblox.com"
	    chatUser.password = params.password
	    console.log(chatUser)
	QB.chat.connect(chatUser,function(err,roster){
	    //on message listener that performs callback when we get a msesage
	    QB.chat.onMessageListener = function(userID,message){
		newMessageFactory.newMessage.message = message 
		console.log(message)
		newMessageFactory.newMessage.userID = userID 
		//we need to update the message lists
		for (var i = 0, l = matchMessagesFactory.matchMessages.length; i < l; i ++) {
		    if(matchMessagesFactory.matchMessages[i].dialogID === message.extension.dialog_id){
			//change last message sent
			matchMessagesFactory.matchMessages[i].showMessage = message.body
			matchMessagesFactory.matchMessages[i].isUnread = true
			match_message = matchMessagesFactory.matchMessages[i]
			//remove message from array and readd to top
			matchMessagesFactory.matchMessages.splice(i,1)
			matchMessagesFactory.matchMessages.unshift(match_message)
			}
		}
		$rootScope.$emit('newMessage')
		}
	    })

	    })
		// console.log(post_data)
                // $http.post(RootSettings.url + '/userinfo/setquickbloxid',post_data).success(function(data){
		//     console.log(data)
		//     RootSettings.userDetails.quickbloxID = data.data

		 // })
// // QB.chat.connect(chatUser,function(err,roster){
	    // //on message listener that performs callback when we get a msesage
	    // QB.chat.onMessageListener = function(userID,message){
		// newMessageFactory.newMessage.message = message 
		// console.log(userID)
		// console.log(message)
		// newMessageFactory.newMessage.userID = userID 
		// //we need to update the message lists
		// for (var i = 0, l = matchMessagesFactory.matchMessages.length; i < l; i ++) {
		//     console.log(matchMessagesFactory.matchMessages[i].dialogID )
		//     console.log(message.extension.dialog_id)
		//     if(matchMessagesFactory.matchMessages[i].dialogID === message.extension.dialog_id){
		// 	//change last message sent
		// 	matchMessagesFactory.matchMessages[i].showMessage = message.body
		// 	matchMessagesFactory.matchMessages[i].isUnread = true
		// 	match_message = matchMessagesFactory.matchMessages[i]
		// 	//remove message from array and readd to top
		// 	matchMessagesFactory.matchMessages.splice(i,1)
		// 	matchMessagesFactory.matchMessages.unshift(match_message)
		// 	console.log('wedidit')
		// 	console.log(matchMessagesFactory.matchMessages)
		// 	}
		// }
		// $rootScope.$emit('newMessage')
		// }
	    // })

                 
	    })

	    }
	    //connect to chat
	    	//get into chat server
	    })

            })
      }
      }
           

      })  

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };
$scope.swipeRight
  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
  //check to see if user is logged in. If not then we must redirect to signup page
  $scope.checkLogin = function(){
  }
})


//***********************************************************************************
//CONTROLLER THAT HANDLES ALL LOGINS AND REGISTERS
.controller('authenticationCtrl', function($scope,$state, $http, RootSettings,$rootScope,$location,$window) {
    $scope.facebookLoggedIn = false
    $scope.systemLoggedIn = false
    //user details
    $scope.facebookUserDetails = {}
    //show loading gif
    $scope.showLoading = false
    $scope.hideLogin = false
    $scope.geoLocationOptions =  { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true }
    //FUNCTION used to get geolocation success
    $scope.geoLocationSuccess = function(position){

      RootSettings.currentLatitude = position.coords.latitude
       RootSettings.currentLongitude = position.coords.logitude
    }

     //FUNCTION used to get geolocation success
    $scope.geoLocationError = function(data){
	console.log(data)
$location.path('/') 
	$window.location.reload()

    }

//FUNCTION USED TO LOGIN USER
    $scope.login = function() {
      // From now on you can use the Facebook service just as Facebook api says
      //show loading spinned
    $scope.facebookUserDetails={}
     
       openFB.login(function(response) {
       	 $scope.showLoading = true
      $scope.hideLogin = true
       
         if (response.status === 'connected') {
                console.log('Facebook login succeeded');
                $scope.closeLogin();}
		else{
			$scope.showLoading = false
		$scope.hideLogin = false
		$scope.showError = true
		}
        $scope.facebookUserDetails.accessToken = response.authResponse.token

        //GET USER DATA from facebook
        openFB.api({
          path: "/me",
          success: function (response) {
            if (response && !response.error) {
              $scope.facebookUserDetails.first_name = response.first_name
              $scope.facebookUserDetails.last_name = response.last_name
              $scope.facebookUserDetails.gender = response.gender
              $scope.facebookUserDetails.link = response.link
              $scope.facebookUserDetails.userID = response.id
              $scope.facebookUserDetails.timezone = response.timezone
              $scope.facebookUserDetails.locale = response.locale
	openFB.api({
          path:"/me/picture",
          params:{width:500,redirect:false},
          success: function (response) {
            console.log('trolo')
            if (response && !response.error) {
              console.log(response)
              $scope.facebookUserDetails.profile_picture_url = response.data.url
            }
          
	navigator.geolocation.getCurrentPosition(function(position){
          RootSettings.currentLongitude=position.coords.longitude
          RootSettings.currentLatitude=position.coords.latitude
          post_data = {}
          post_data.facebookUserDetails = $scope.facebookUserDetails
          post_data.currentLongitude = RootSettings.currentLongitude.toString()
          post_data.currentLatitude = RootSettings.currentLatitude.toString()
        //make a post to signup user. If they are already signed up then redirect 
        console.log(post_data)
        // them to their own dashboard.
        $http.post(RootSettings.url +'/userinfo/usersignup',post_data).
        //if signup was successful
          success(function(data){
            console.log(data)
            accessToken = data.access_token
            $scope.facebookUserDetails.accessToken = data.access_token
	    $scope.facebookUserDetails.id = data.id
            //set long token that will last 60 days
            window.sessionStorage['fbtoken'] = data.access_token
            RootSettings.userDetails = $scope.facebookUserDetails
                
            RootSettings.userDetails.address= data.address
	    RootSettings.userDetails.email = data.email
            //get user type
            user_type = data.user_type
	    $scope.showLoading = false
	    $scope.hideLogin = false

            if (user_type === "employee"){
            	$state.go('app.employeeIntro',{}, {reload: true})
              //$state.go('employee.searchJobs')
            }else if(user_type === "employer"){
            	$state.go('app.employerIntro',{}, {reload: true})
              //$state.go('employer.employerDashboard')
            }else{
              $state.go('app.chooseUserType')
            }



          }).error(function(data){
            console.log(data)
          })
       

      },$scope.geoLocationError,$scope.geoLocationOptions);

  
	  
	  
	  
	  }





      });



            }
          }
        });

        //GET USER PROFILE
                 //get location, if location get is successful then signuo
        
        }, {scope: 'email,user_photos,user_work_history'})
        // try to sign up user if they dont exist.If they do, then route to specific
        //dash boards
        //set up post data
        
    };

    $scope.getLoginStatus = function() {
      //Check to see if logged in using facebook
      openFB.getLoginStatus(function(response) {
        //get auth token
        console.log(response)
        if(response.status === 'connected') {
           var token = response.authResponse.token
          //get details of user
          openFB.api({
          path: '/me',
          success :function (response) {
            $scope.facebookUserDetails.userID = response.id
            $scope.facebookUserDetails.first_name = response.first_name
            $scope.facebookUserDetails.last_name = response.last_name
            $scope.facebookUserDetails.gender = response.gender
            $scope.facebookUserDetails.link = response.link
            $scope.facebookUserDetails.timezone = response.timezone
            $scope.facebookUserDetails.locale = response.locale
	    RootSettings.userDetails = $scope.facebookUserDetails
            var userRole = ""
            $http.get(RootSettings.url +'/userinfo/checkfacebooktoken?facebook_short_token='+ token+'&socialUserID=' + $scope.facebookUserDetails.userID).
            then(function(data){
              $scope.facebookloggedIn = true
              RootSettings.userDetails.accessToken = data.data.access_token
              RootSettings.userDetails.id = data.data.id
              window.sessionStorage['fbtoken'] = data.access_token

              //Get user role (i.e) employee or employer
               $http.get(RootSettings.url+ '/userinfo/getuserdata?accessToken='+RootSettings.userDetails.accessToken+'&socialUserID='+$scope.facebookUserDetails.userID ).
                  then(function(data){
                    //returns "employer" or "employee"
		   accessToken = data.accessToken
            RootSettings.userDetails.accessToken = accessToken
	    RootSettings.userDetails.id = data.id
            //set long token that will last 60 days
            window.sessionStorage['fbtoken'] = data.accessToken
	    RootSettings.userDetails.address= data.address
	    RootSettings.userDetails.userType= data.userType
            //get user type
                    userRole = data.userType
                     //Check to see user roles
                    if( userRole === 'employer'){
                      //if user is employer we need to redirect them to dashboard
                      $state.go('employer.employerDashboard') 
                    }else if (userRole === 'employee'){
                      //if user is employee, redirect them to search for jobs
                       $state.go('employee.searchJobs') 
                    }else{
                      //if user hasnt picked a role
                       $state.go('app.loginPage') 
                      }
                  })
              })
              },
              error: function(data){
                $state.go('app.loginPage') 
              }

            })

        } else {
          $scope.facebookLoggedIn = false;
          console.log('hi')
          $state.go('app.loginPage') 
        }
      })

      $rootScope.$on('restart',function() {
      	$scope.getLoginStatus()
      })
      
    
 
        // $location.path("/#/app/loginpage");
        //$route.current = "/loginpage"
    
       
    };

    $scope.me = function() {
      Facebook.api('/me', function(response) {
        $scope.user = response;
      });
    };

  })
//************************************************************************
//CONTROLLER used to deal with adding information upon first login
.controller('userSignupCtrl',function($scope,$state,$http, pictureService, RootSettings){
  $scope.userType =""
  $scope.showLoading = true
  $scope.showPageContent = false
  //jobtypes returned by server
  $scope.jobTypes = []
  //jobtypes that the user is part of
  $scope.selectedJobTypes =[]
  //choice that employee makes for wage
  $scope.wageExpectations = 0
  $scope.profilePicture = ""
  //user details
  $scope.userDetails = RootSettings.userDetails
  $scope.companyName=""
  $scope.companyDescription=""

  $scope.profilePictureUrl = ""
    $scope.init = function() {
    }  
    
  //FUNCTION USED TO CHOOSE EMPLOYEE FOR USER
  $scope.chooseUserType = function(userType){
    $scope.userType = userType
    post_data = {}
    post_data.socialUserID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    post_data.userType = userType
    $http.post(RootSettings.url + "/userinfo/chooseusertype", post_data)
      .success(function(data){
        if ($scope.userType  === "employee"){
           $state.go('app.employeeIntro')
         }else {
          $state.go('app.employerIntro')
         }
       

      }).error(function(data){
        console.log(data)
        $state.go('app.loginPage') 
      })
  }
  //FUNCTION redirect to change profile picture view            
  $scope.changeCompanyProfilePicture= function(){
    $state.go('changepicture.changeProfilePicture')
  }    
  //GET all the job classes for use  
  $scope.employeeInit  = function(){
      $http.get(RootSettings.url +'/jobs/getjobclasses')
                    .then(function(data){
                      $scope.jobTypes= data.data
                      $scope.showLoading = false

                      console.log($scope.jobTypes)
                    })
                    $scope.showPageContent = true
                  }
  $scope.employerInit  = function(){
      $scope.showLoading = false
      $scope.showPageContent = true
      $scope.companyName = RootSettings.userDetails.first_name +" "+ RootSettings.userDetails.last_name
      $scope.profilePictureUrl = RootSettings.userDetails.profile_picture_url
                  }
      $scope.profilePictureList=[]            
            

  //FUNCTION that deals with selecting job types user is interested in
  $scope.selectJobType = function(jobtype_id) {
      var idx = $scope.selectedJobTypes.indexOf(jobtype_id);

      // is currently selected
      if (idx > -1) {
        $scope.selectedJobTypes.splice(idx, 1);
      }

      // is newly selected
      else {
        $scope.selectedJobTypes.push(jobtype_id);
      }
      console.log($scope.selectedJobTypes)
    };
  //FUNCTION USED TO SIGN UP USER DURING FIRST SIGNUP
  $scope.createEmployeeBaseData = function(){
    //get the values for wage expectations
    var salaryLow = 0
    var salaryHigh = 9999

    if($scope.wageExpectations === 1 ){
      salaryHigh = 50
    }
    else if ($scope.wageExpectations === 2){
      salaryLow =50
      salaryHigh = 100

    } else if ($scope.wageExpectations === 3){
      salaryLow = 100
      salaryHigh = 9999
    }
    //prepare post_data
    post_data = {}
    post_data.salaryLow = salaryLow
    post_data.salaryHigh = salaryHigh
    post_data.selectedJobTypes = $scope.selectedJobTypes
    post_data.socialUserID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    $http.post(RootSettings.url + "/userinfo/employeefirststep",post_data).
      success(function(data){
        $state.go('app.employeeIntro') 
      }).error(function(data){

      })
  }

  //FUNCTION USED TO CREATE EMPLOYER BASE DATA
  $scope.createEmployerBaseData=function(){
    post_data = {}
    post_data.socialUserID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    post_data.companyName = $scope.companyName
    post_data.companyDescription = $scope.companyDescription
    $http.post(RootSettings.url + "/userinfo/employerfirststep",post_data).
      success(function(data){
        $state.go('app.employerIntro') 
      }).error(function(data){

      })

  }
}) 

//*************************************************************
  //CONTROLLER used to change user info i.e profile picutre
.controller('changeUserDataCtrl',function($scope,$ionicLoading,$state,$http, pictureService, RootSettings){ 
    $scope.profilePictureList = []
    $scope.profileAfterId = ""
    //show the thumbnails
    $scope.photosOfYouThumbnail =""
    $scope.showPictureList= true
    //show Display Pictures
    $scope.showDisplayPictures = false
    $scope.displayPictureChoices = []
    $scope.displayType = ""
    //FUNCTIOn used to show which pictures to dispaly
    $scope.showDisplayPictureType = function(type){
      if (type ==="profile"){
        $scope.displayType = type
        $scope.displayPictureChoices = $scope.profilePictureList
         $state.go('changepicture.choosePicture',{'type':'profile'});
      }

    }

    

      //FUNCTION used to change company profile picture                
  $scope.changeProfilePicture= function(after_id){
    //full loading scsreen on initial loading
    if(after_id === null){
       $ionicLoading.show({
        templateUrl: 'templates/loading.html',
        noBackdrop:true
      });
    }
    pictureService.getFBProfiles(after_id).then(function(data){
      console.log(data)
      $scope.profilePictureList = data.returnProfilePictures
      $scope.photosOfYouThumbnail = $scope.profilePictureList[0].thumbnail 
    })
    //full loading scsreen on initial loading
    if(after_id === null){
       $ionicLoading.hide();
    }
   


  }    

$scope.$on('$stateChangeSuccess', function() {
    // $scope.loadMorePictures();
  });
})




.controller('pictureSelectorCtrl',function($scope,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, pictureService, RootSettings){
  $scope.displayPictureChoices = []
  $scope.pictureAfterId=""
  $scope.albumType = ""
  
  $scope.cropImage= function(picture_id){
    $state.go("changepicture.pictureCrop", {'id':picture_id})
  }
  // FUNCTION initial load depending on album
  $scope.loadInit=function(){
    $scope.albumType = $stateParams.type
    if($scope.albumType === "profile"){
        var after_id = null
       if(after_id === null){
       $ionicLoading.show({
        templateUrl: 'templates/loading.html',
        noBackdrop:true
      });
    }
    pictureService.getFBProfiles(after_id).then(function(data){
      console.log(data)
      $scope.displayPictureChoices = data.returnProfilePictures
      $scope.pictureAfterId=data.afterID
    })
    //full loading scsreen on initial loading
    if(after_id === null){
       $ionicLoading.hide();
    }

    }
  }

//ajax infinite load
    $scope.loadMorePictures = function(){

      if ($scope.albumType ==="profile"){
          pictureService.getFBProfiles($scope.pictureAfterId ).then(function(data){
            for(i = 0; i < data.returnProfilePictures.length; i++){
               $scope.displayPictureChoices.push(data.returnProfilePictures[i])
            }
            $scope.pictureAfterId = data.afterID
            $scope.$broadcast('scroll.infiniteScrollComplete');
    })
    }

    }  
    //FUNCTION chooser    
    $scope.chooseImage = function(picture_id){
     console.log(picture_id) 
      pictureService.getFBPicture(picture_id ).then(function(data){
          //change the profile picture
           var pictureUrl = data  
           post_data = {}
           post_data.socialUserID = RootSettings.userDetails.userID
           post_data.accessToken = RootSettings.userDetails.accessToken
           post_data.pictureUrl = pictureUrl
           //make a post request to change profile picture
	    $state.go('changepicture.picturecrop', {id:picture_id})

      })

    }


})
//********************************************************
//CONTROLLER used to control all the menu options for employee
.controller('employeeMenuCtrl',function($scope, $ionicLoading, $ionicSideMenuDelegate,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, pictureService, RootSettings){
//FUNCTION ON SIDE BAR USED TO GET PROFILE INFO
$scope.getProfile = function(){
    id = RootSettings.userDetails.id
    $state.go('employee.viewProfile', {id:id})
    }
//FUNCTION TO EDIT PROFILE INFORMATION    
    $scope.editProfile = function(){
	$state.go('employee.editProfile')
    }
//FUNCTION USED TO GET DISCOVERY PREFERENCES
    $scope.changeDiscoveryPreferences = function() {
    	$state.go('employee.changeDiscoveryPreferences')
    }
    $scope.updateAddress = function() {
    	$state.go('employee.updateAddress')
    }
$scope.goEmployerDash = function() {
	var userType ="employee"
	params = {}
	    params['userID'] = RootSettings.userDetails.userID
		params['accessToken']= RootSettings.userDetails.accessToken
		params['userType'] = userType
		$http.get(RootSettings.url + '/userinfo/hasusertype',{params:params}).success(function(data){
		    RootSettings.userDetails.userType = "employer"
		    //$state.go('employer.employerDashboard')
		    $state.go('app.employerIntro',{}, {reload: true})

}).error(function() {
    //if return error, then no employer data was created so we must 
    //create the data and then route to the correct pages
    post_data = {}
    post_data.socialUserID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    if (userType === "employee"){
	post_data.userType = "employer"
    }else if (userType === "employer"){
	post_data.userType = "employee"
    }
    $http.post(RootSettings.url + "/userinfo/chooseusertype", post_data)
      .success(function(data){
        if (userType  === "employee"){
           $state.go('app.employerIntro',{}, {reload: true})
         }else {
          $state.go('app.employeeIntro',{}, {reload: true})
         }
      }).error(function(data){
        console.log(data)
      })

})
}

})

//********************************************************
//CONTROLLER used to control all the menu options for employer
.controller('employerMenuCtrl',function($scope, $ionicLoading, $ionicSideMenuDelegate,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, pictureService, RootSettings){
   //toggle left side menu
   //
$scope.getProfile = function(){

    id = RootSettings.userDetails.id
    $state.go('employer.viewProfile', {id:id})
    }
    $scope.editProfile = function(){
	$state.go('employer.editProfile')
    }
//Function used to get discovery preference
 
 $scope.createNewJob = function() {
  $state.go('employer.createJob')
  };

 //toggle left side menu
 $scope.toggleLeftSideMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
  //toggle right side menu
   $scope.toggleRightSideMenu = function() {
    $ionicSideMenuDelegate.toggleRight();
  };
  //toggle wipe right
 $scope.onSwipeRight = function() {
 $ionicSideMenuDelegate.hideLeft();
}

$scope.goEmployeeDash = function() {
	var userType ="employer"
	params = {}
	    params['userID'] = RootSettings.userDetails.userID
		params['accessToken']= RootSettings.userDetails.accessToken
		params['userType'] = userType
		$http.get(RootSettings.url + '/userinfo/hasusertype',{params:params}).success(function(data){
		    RootSettings.userDetails.userType = "employee"
		   // $state.go('employee.searchJobs')
		    $state.go('app.employeeIntro',{}, {reload: true})


}).error(function() {
    //if return error, then no employer data was created so we must 
    //create the data and then route to the correct pages
    post_data = {}
    post_data.socialUserID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    if (userType === "employee"){
	post_data.userType = "employer"
    }else if (userType === "employer"){
	post_data.userType = "employee"
    }
    $http.post(RootSettings.url + "/userinfo/chooseusertype", post_data)
      .success(function(data){
        if (userType  === "employee"){
           $state.go('app.employerIntro',{}, {reload: true})
         }else {
          $state.go('app.employeeIntro',{}, {reload: true})
         }
      }).error(function(data){
        console.log(data)
      })

})
}

})
//************************************************************************
//CONTROLLER FOR EMPLOYER DASHBOARD
.controller('employerDashboardCtrl',function($scope, $ionicLoading, $ionicSideMenuDelegate,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, pictureService, RootSettings,quickbloxService,$rootScope,newNotificationService,$interval){
 $scope.jobs = []
 //variable used to show which job to goto
$scope.showNoJobs = false
$scope.showLoading = false
//FUNCTION init function of employer
 $scope.init = function(){
    $scope.showLoading = true
     //update current lat and long
    navigator.geolocation.getCurrentPosition(function(position){
                RootSettings.currentLongitude=position.coords.longitude
                RootSettings.currentLatitude=position.coords.latitude
		//set long and lat
		post_data = {}
		 post_data.currentLatitude =position.coords.latitude.toString()
                post_data.currentLongitude= position.coords.longitude.toString()
                post_data.userID = RootSettings.userDetails.userID
                post_data.accessToken = RootSettings.userDetails.accessToken
                $http.post(RootSettings.url + '/userinfo/updatelocation',post_data).success(function(data){
		    console.log('Location Updated')
		    })
    }) 
        //Set interval to check for new notifications
    $interval(function() {
	newNotificationService.checkNewMatches()
	newNotificationService.checkNewMessages()
    },60000)
    newNotificationService.checkNewMatches()
    newNotificationService.checkNewMessages()

   quickbloxService.connectToQB()
   //set RootSettings user type to employer 
    RootSettings.userDetails.userType = "employer"
    $http({url:RootSettings.url + '/jobs/getemployerjobs',
          method:"GET",
          params:{'socialUserID':RootSettings.userDetails.userID,'accessToken':RootSettings.userDetails.accessToken},
      }).then(function(data){
      //check to see if any jobs are present  
      if(data.data.length === 0){
        $scope.showNoJobs = true
      }
      $scope.jobID = $stateParams.id
      for( var x = 0 ; x < data.data.length ; x++){
	//check to see if there is a new job, if there is, change colour
	if (parseInt(data.data[x].num_new_applicants) > 0){
	    data.data[x].hasNew = true
	    }
        $scope.jobs.push(data.data[x])
      }
      console.log($scope.jobs)
      //iterate through jobs only if we get a return job id to show that
      // we are coming from the create job page
      if (typeof $scope.jobID != 'undefined'){
        for(var i = 0 ; i < $scope.jobs.length ; i++){
          if ($scope.jobs[i].id == $scope.jobID){
            $scope.jobs[i].isNew =  true
          }
        }
      }

    $scope.showLoading = false

    })
  }

  //FUNCTION: used to direct employer to job page to see all users that have
  //swiped right to their job
  $scope.selectJob = function(job_id){
    $state.go('employer.getJobEmployees',{'id':job_id})
  }
 // open up all the job matches
  $scope.openJobMatches = function(){
      newNotificationService.setNewMatches(false)
      $rootScope.$emit('refreshMatches')
    $state.go('employer.getJobMatches')
  
  }
  //FUNCTION: Pull to refresh
  $scope.refreshJobs = function() {
      //re get jobs
      $scope.jobs=[]
    $http({url:RootSettings.url + '/jobs/getemployerjobs',
          method:"GET",
          params:{'socialUserID':RootSettings.userDetails.userID,'accessToken':RootSettings.userDetails.accessToken},
      }).then(function(data){
      //check to see if any jobs are present  
      if(data.data.length === 0){
        $scope.showNoJobs = true
      }else{
      $scope.showNoJobs = false
      }
      $scope.jobID = $stateParams.id
      for( var x = 0 ; x < data.data.length ; x++){
	//check to see if there is a new job, if there is, change colour
	if (parseInt(data.data[x].num_new_applicants) > 0){
	    data.data[x].hasNew = true
	    }
        $scope.jobs.push(data.data[x])
      }
      $scope.$broadcast('scroll.refreshComplete');
          
      })
 
  }
//FUNCTION remove num appl;icants from job	
$rootScope.$on('employerSwiped', function(event,args) {
jobID = args.job_id
console.log(jobID)
    for(var i = 0; i < $scope.jobs.length; i++){
	if($scope.jobs[i].id=== jobID){
	    //remove 1 from num_applicants
	    $scope.jobs[i].num_new_applicants = $scope.jobs[i].num_new_applicants - 1
	    if($scope.jobs[i].num_new_applicants === 0){
	     $scope.jobs[i].hasNew= false
	    }
	}

    }
})
//if job has been edited
$rootScope.$on('jobEdited', function(event,args) {
$scope.refreshJobs()
})
//FUNCTION: Used to remove job from list
$rootScope.$on('deleteJob', function(event,args) {
   jobID = args.job_id
    for(var i = 0; i < $scope.jobs.length; i++){
	console.log($scope.jobs[i].id)
	if($scope.jobs[i].id=== jobID){
	    $scope.jobs.splice(i,1)
	}
    }
	
} )

$rootScope.$on('newMessage',function() {
	if($state.current.name!='messages.messagesDashboard' ){
                 newNotificationService.setNewMessages(true)
                     }
});

})
//**************************************************************************
//CONTROLLER USED TO GET ALL THE MATCHES
.controller('jobMatchCtrl',function($scope, $ionicLoading, $ionicSideMenuDelegate,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, pictureService, RootSettings,quickbloxService,$rootScope,$ionicPopup){
 $scope.regularMatches = []
 $scope.guaranteeJobMatches = []
 $scope.showLoading = false
 //variable used to show which job to goto
$scope.showNoMatches = false
//FUNCTION init function of employer
 $scope.getJobMatches = function(){
     
     $scope.showLoading = true
$scope.regularMatches = []
 $scope.guaranteeJobMatches = []
    params = {}
    params['userID'] = RootSettings.userDetails.userID
    params['accessToken']= RootSettings.userDetails.accessToken
    params['userType'] = RootSettings.userDetails.userType
    $http.get(RootSettings.url + '/jobs/getmatches',{params:params}).success(function(data){
	console.log(data)
	//if there are no jobs present
	if(data.length === 0 ){
	$scope.showNoMatches = true
	$scope.showLoading = false
	return false
	}
	//we need to get the guaranteed matches and non guarantee matches
	var returnMatches = data
	for (var i = 0, l = returnMatches.length; i < l; i ++) {
		var match = returnMatches[i];
		match.dateMatched= moment(match.dateAdded).format("MMM D")
		//job is guarantee pay
		if (match.isGuaranteePay === true){
		    $scope.guaranteeJobMatches.push(match)
		    //job is not guarantee pay
		}else{
		    $scope.regularMatches.push(match)	
		}
		$scope.showLoading = false

	}
    }).error(function(data){
	$state.go('app.startup')
    }) .finally(function() {
       // Stop the ion-refresher from spinning
     });
 }


 // FUNCTION Refresh matches
 $scope.refreshMatches =function() {
 $scope.regularMatches = []
 $scope.guaranteeJobMatches = []
    params = {}
    params['userID'] = RootSettings.userDetails.userID
    params['accessToken']= RootSettings.userDetails.accessToken
    params['userType'] = RootSettings.userDetails.userType
    $http.get(RootSettings.url + '/jobs/getmatches',{params:params}).success(function(data){
	console.log(data)
	//if there are no jobs present
	if(data.length === 0 ){
	$scope.showNoMatches = true
	return false
	}
	//we need to get the guaranteed matches and non guarantee matches
	var returnMatches = data
	for (var i = 0, l = returnMatches.length; i < l; i ++) {
		var match = returnMatches[i];
		match.dateMatched= moment(match.dateAdded).format("MMM D")
		//job is guarantee pay
		if (match.isGuaranteePay === true){
		    $scope.guaranteeJobMatches.push(match)
		    //job is not guarantee pay
		}else{
		    $scope.regularMatches.push(match)	
		}
		$scope.showLoading = false

       $scope.$broadcast('scroll.refreshComplete');
	}
    }).error(function(data){
	$state.go('app.startup')
    }) .finally(function() {
       // Stop the ion-refresher from spinning
	
       $scope.$broadcast('scroll.refreshComplete');
     });
	
 } 
 //FUNCTION: offer match to employee
   $scope.offerMatch = function(matchID, matchType){
    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Once you offer this job to a user, you cannot pull back your offer. Once they accept this offer, your job will be pulled from the job pool"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		$http.post(RootSettings.url + "/jobs/offermatch", post_data).success(function(data){
		    //if matchtype is guarantee pay, we need to edit item from guarantee pay. If not we need to edit from regular
		if (matchType === "regular"){
		    for (var i = 0, l = $scope.regularMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.regularMatches[i].id === matchID) {
			    $scope.regularMatches[i].isOffered = true
			    $scope.regularMatches[i].isMatched = false
			    //move to top of list
			    moveMatch = $scope.regularMatches[i]
			    $scope.regularMatches.splice(i,1)
			    $scope.regularMatches.unshift(moveMatch)
			    return
			}
		    }
		}
		    else if (matchType ==="guarantee"){
		for (var i = 0, l = $scope.guaranteeJobMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.guaranteeJobMatches[i].id === matchID) {
			    $scope.guaranteeJobMatches[i].isOffered = true
			    $scope.guaranteeJobMatches[i].isMatched = false
			    //move to top of list
			    moveMatch = $scope.guaranteeJobMatches[i]
			    $scope.guaranteeJobMatches.splice(i,1)
			    $scope.guaranteeJobMatches.unshift(moveMatch)

			    return
			}

		    }
		}
		})
	    }else{
	    return false
	    }
       })
       }
//FUNCTION : accept match 
 $scope.acceptOffer = function(matchID,matchType){
     //if there is no postal code and it is a guarantee job, ask for postal code
     if (matchType === "guarantee" && RootSettings.userDetails.address.postalCode === null){
	//check to see if there is a postal code so we can mail the check
	if(RootSettings.userDetails.address.postalCode === null){
	    var confirmPopup = $ionicPopup.confirm({
            title: 'You Are About to Accept A "Guarantee Pay" Job ',
            template: "In order to pay you for a guarantee pay job, we will need your address and postal code so that we can mail you the cheque "
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
	    $state.go('employee.updateAddress')
	}else{
	    return
	}
     })
	}
	return
     }
    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Are you sure you want to accept this job? Once you accept this job you cannot unmatch"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		$http.post(RootSettings.url + "/jobs/acceptoffer", post_data).success(function(data){
		if (matchType === "regular"){
		    for (var i = 0, l = $scope.regularMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.regularMatches[i].id === matchID) {
			    $scope.regularMatches[i].isHired = true
			    $scope.regularMatches[i].isOffered= false
			     //move to top of list
			    moveMatch = $scope.regularMatches[i]
			    $scope.regularMatches.splice(i,1)
			    $scope.regularMatches.unshift(moveMatch)

			    return
			}
		    }
		}
		    else if (matchType ==="guarantee"){
		for (var i = 0, l = $scope.guaranteeJobMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.guaranteeJobMatches[i].id === matchID) {
			    $scope.guaranteeJobMatches[i].isHired = true
			    $scope.guaranteeJobMatches[i].isOffered = false
			    moveMatch = $scope.guaranteeJobMatches[i]
			    $scope.guaranteeJobMatches.splice(i,1)
			    $scope.guaranteeJobMatches.unshift(moveMatch)

			    return
			}

		    }
		}

		})
	    }else{
	    return false
	    }
       
       })
 
 }

//ASIGN JOB TO USER
    $scope.removeMatch = function(matchID,matchType){
    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Are you sure you want to remove this match"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		post_data.userType = RootSettings.userDetails.userType
		$http.post(RootSettings.url + "/jobs/removematch", post_data).success(function(data){
		 if (matchType === "regular"){
		    for (var i = 0, l = $scope.regularMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.regularMatches[i].id === matchID) {
			    $scope.regularMatches.splice(i,1)
			    return
			}
		    }
		}
		    else if (matchType ==="guarantee"){
		for (var i = 0, l = $scope.guaranteeJobMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.guaranteeJobMatches[i].id === matchID) {
			    $scope.guaranteeJobMatches.splice(i,1)
			    return
			}

		    }
		}
   
		})

	    //if user declines
	    }else{
	    return
	    }
       })
    }
//FUNCTION : accept match 
 $scope.jobCompleted = function(matchID,matchType){
    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Are you sure this job has been completed?"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		$http.post(RootSettings.url + "/jobs/completejob", post_data).success(function(data){
		if (matchType === "regular"){
		    for (var i = 0, l = $scope.regularMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.regularMatches[i].id === matchID) {
			    $scope.regularMatches[i].isHired = false
			    $scope.regularMatches[i].isCompleted= true
			     //move to top of list
			    moveMatch = $scope.regularMatches[i]
			    $scope.regularMatches.splice(i,1)
			    $scope.regularMatches.unshift(moveMatch)

			    return
			}
		    }
		}
		    else if (matchType ==="guarantee"){
		for (var i = 0, l = $scope.guaranteeJobMatches.length; i < l; i ++) {
			//if that was the job wthat was offefrred, transform to offer
			if($scope.guaranteeJobMatches[i].id === matchID) {
			    $scope.guaranteeJobMatches[i].isHired = false
			    $scope.guaranteeJobMatches[i].isCompleted = true
			    //move match to top
			     moveMatch = $scope.guaranteeJobMatches[i]
			    $scope.guaranteeJobMatches.splice(i,1)
			    $scope.guaranteeJobMatches.unshift(moveMatch)

			    return
			}

		    }
		}

		})
	    }else{
	    return false
	    }
       
       })
 
 }

 $rootScope.$on('refreshMatches',function() {
  $scope.refreshMatches()

 })

})



//*********************************************************************************
//CONTROLLER THAT DEALS WITH EDITING JOB PROFILE
.controller('editJobCtrl',function($scope, $ionicLoading, $ionicSideMenuDelegate,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, pictureService, RootSettings,quickbloxService, $ionicHistory,$rootScope){
$scope.job = {}
$scope.jobClasses =[]
// init function that will do the initializing of the edit profile
$scope.init = function(){
    //get job classes
    
    var jobID = $stateParams.id
    params = {}
    params.jobID = jobID
    params.userID = RootSettings.userDetails.userID
    params.accessToken = RootSettings.userDetails.accessToken
    $http.get(RootSettings.url + '/jobs/editjob',{params:params}).success(function(data){
	console.log(data)
	$scope.job= data
    })
$http.get(RootSettings.url +'/jobs/getjobclasses')
        .then(function(data){
          $scope.jobClasses= data.data
	})
   

    
    }

    //function used to save job
    $scope.saveJob = function() {
    params = {}
    post_data.job = $scope.job
    post_data.userID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    $http.post(RootSettings.url + '/jobs/editjob',post_data).success(function(data){
	$rootScope.$emit('jobEdited')
	$ionicHistory.goBack()	
    })


    
    }

})
//************************************************************************************ 
//CONTROLLER THAT DEALS WITH CROPPING IMAGES
.controller('pictureCropCtrl',function($scope,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, pictureService, RootSettings,$jrCrop,profileDataFactory,$rootScope){
  $scope.pictureId = ""
  $scope.pictureUrl =""
  $scope.myImage='';
  $scope.myCroppedImage='';
  $scope.value = ""
  $scope.$watch('myCroppedImage',function(){
  })

  //Init load
  $scope.loadInit= function(){
    $scope.pictureId = $stateParams.id

     pictureService.getFBPicture($scope.pictureId ).then(function(data){
            console.log(data)
            $scope.myImage= data   
			   })
  }

$scope.logit = function() {
	console.log($scope.value)
}
  $scope.cropPicture = function() {
      //data value is in last bucket for some stupid reason
      var dataclass = document.getElementsByClassName('data-val')
      var bucket = dataclass.length - 1
      var dataurl = dataclass[bucket].value
  	post_data = {}
	post_data.accessToken =  RootSettings.userDetails.accessToken
        post_data.userID = RootSettings.userDetails.userID
	post_data.image = dataurl 
	$http.post(RootSettings.url + '/userinfo/croppicture',post_data).success(function(data){
	    console.log(data)
	    RootSettings.userDetails.profilePictureUrl = data
	    userType = RootSettings.userDetails.userType
	    profileDataFactory.profile.profilePictureUrl = data
	    $rootScope.$broadcast('profileUpdated')
	    	})
  }
  $scope.goToState = function(){
      userType = RootSettings.userDetails.userType
      id = RootSettings.userDetails.id

  if(userType === "employee") {
		$state.go('employee.viewProfile', {id: id})
	    }else if (userType === "employer"){
		$state.go('employer.viewProfile',{id:id},{reload:true})
	    }

  }
  

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }

   $scope.$on("cropme:done", function(e, result, canvasEl) {
        // var xhr = new XMLHttpRequest;
        // xhr.setRequestHeader("Content-Type", blob.type);
        // xhr.onreadystatechange = function(e) {
        //     if (this.readyState === 4 && this.status === 200) {
        //         return console.log("done");
        //     } else if (this.readyState === 4 && this.status !== 200) {
        //         return console.log("failed");
        //     }
        // };
        // xhr.open("POST", url, true);
        // xhr.send(blob);
    });
})  

//***********************************************************************************************
//CONTROLLER USED BY EMPLOYER FOR CREATING JOBS
.controller('createJobCtrl',function($scope,locationService,$state,$stateParams,$http,$ionicLoading, $ionicNavBarDelegate, RootSettings,$ionicPopup,$ionicPopover){
  $scope.jobClasses = []
  $scope.job = {}
  $scope.address = {}
  $scope.job.pay = null
  $scope.jobPay = ""
  $scope.jobClassSelected = 0
  $scope.jobTitle = ""
  $scope.jobDescription = ""
  $scope.jobAddress = ""
  $scope.useJobAddress = false
  $scope.jobAddress = ""
  $scope.jobCity=""
  $scope.jobRegion=""
$scope.allowAddress = true
  //allow guarantee pay
$scope.guaranteePayWage = null
$scope.allowGuaranteePay = true
$scope.useGuaranteePay = false
$scope.stripeToken = ""
//showing addresses and such
$scope.showCanadaRegions =  false
$scope.showAmericanRegions = false
//creadit card stuff
$scope.card = {}
  //Error message stuff
  $scope.showError = false
  $scope.errorMessage = ""
  //FUNCTION init to get all of the job classes
  $scope.init = function(){
      //get address
      $scope.address= RootSettings.userDetails.address
      console.log(RootSettings.userDetails.address)
      //address hasnt been set yet
      if($scope.address === undefined){
	  $scope.address = {}
      	 $scope.address.country = "CA" 
	$scope.address.region = "BC"
	$scope.showCanadaRegions = true
	$scope.showAmericanRegions = false

      }
      else if($scope.address.city === null ){
	 $scope.address.country = "CA" 
	$scope.address.region = "BC"
	$scope.showCanadaRegions = true
	$scope.showAmericanRegions = false
      }else{
	if ($scope.address.country === "CA"){
	$scope.showCanadaRegions = true
	}else if ($scope.address.country === "US"){
	$scope.showAmericanRegions = true
	}
      }
     $http.get(RootSettings.url +'/jobs/getjobclasses')
        .then(function(data){
          $scope.jobClasses= data.data
          $scope.job.jobClassSelected = $scope.jobClasses[0].id
          })
      }
//FUNCTION USED TO COMPLETE JOB CREATE
   $scope.completeCreateJob = function(post_data) {
	$http.post(RootSettings.url + '/jobs/createjob',post_data).success(function(data){
                  //get job id back
                  job_id = data
		  //reset data
		    $scope.job ={}
		  $scope.isGuaranteePay = false
		  $scope.guaranteePayWage = null
		  $scope.job.pay = null
                  $state.go('employer.employerDashboard',{'id':job_id})
		}).error(function(data){
	    	    haveError = true
		    data = data
		    var errorMessage = "Oops Looks Like We Have A Problem"
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false
		   });
                })
   }
   //function used to check if job is safe to add
   $scope.checkJob= function(type){
    var haveError = false
       var errorMessage = "Oops we have a problem"
    //check to see if user is using address
    var errorMessage= "We have run into an error"
    if($scope.job.title === ""){
	haveError= true
	data = "You must select a title"	
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		   });
	      return false
	    }
   if(($scope.job.pay===0 || $scope.job.pay === null)  && $scope.useGuaranteePay === false){
       haveError = true
	data = "You must pick a wage"	
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false;
		   });
	      return false
	    }else{
 //   if(($scope.guaranteePayWage === 0 || $scope.guaranteePayWage === null) && $scope.useGuaranteePay === true){
 //       haveError = true
	// data = "You must enter the amount you are paying"	
	// var alertPopup = $ionicPopup.alert({
	//         title: errorMessage,
	//         template: data
	//       }).then(function(res) {
	// 	  return false
	// 	   });
 // 		return false
	//     }
	//     //step execution if there os an error
	//     if (haveError === true){
	//     return false
	//     }


	if(type === "regular"){
		$scope.createJob()
	}else{
		$scope.checkGuaranteePayValue()
	}
}
  }

   //FUNCTION USED TO CREATE JOB   
   $scope.createJob = function(){
         //Set up post data
      post_data = {} 
      post_data.job = $scope.job
      post_data.useJobAddress = $scope.useJobAddress
      post_data.socialUserID = RootSettings.userDetails.userID
      post_data.accessToken = RootSettings.userDetails.accessToken
      //check to see if its guarantee pay
      if($scope.useGuaranteePay === true){
      post_data.isGuaranteePay = true
      post_data.job.pay = $scope.guaranteePayWage
      post_data.stripeToken = $scope.stripeToken
      }else{
      post_data.isGuaranteePay = false
      }

      //if user is using address
    if($scope.useJobAddress === true){
	post_data.useAddress= true
	if($scope.address.address === null || $scope.address.address === ""){
	  var data = "Please Enter A Valid Address"
	    var errorMessage = "Oops something went wrong"
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false
		   });

	}else if ($scope.address.city === null || $scope.address.city === ""){
	  var data = "Please Enter A Valid City"
	    var errorMessage = "Oops something went wrong"
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false
		   });

	}
	post_data.address = $scope.address
	//set new address
    urlAddress = $scope.address.address + ','  + 
		$scope.address.region+ "," +
		$scope.address.country
    params = {}
    params.address = urlAddress
    params.sensor= false
    params.key = "AIzaSyDEkwaNbuDVErD5qI1HOcCqfJ9SK2j8U4E"
    $http.get("https://maps.googleapis.com/maps/api/geocode/json?",{params:params}).then(function(res){
	if (res.status != 200){
	    var data = "Please Enter A Valid Address"
	    var errorMessage = "Oops something went wrong"
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false
		   });
 
	}
	//if response is ok
	else{
	    //we need to save last address
	    address_data= {}
	    address_data.socialUserID = RootSettings.userDetails.userID
	    address_data.accessToken = RootSettings.userDetails.accessToken
	    address_data.address= $scope.address
	    $http.post(RootSettings.url + '/userinfo/updateaddress',address_data).success(function() {
	    	
	RootSettings.userDetails.address = $scope.address
	    })
	    $scope.job.latitude = res.data.results[0].geometry.location.lat.toString()
	    $scope.job.longitude = res.data.results[0].geometry.location.lng.toString()
	}
	console.log(post_data)

	$scope.completeCreateJob(post_data)
    })
    
    }else{
	//we are not using address
	post_data.useAddress= false
	$scope.job.latitude = RootSettings.currentLatitude.toString()
		$scope.job.longitude = RootSettings.currentLongitude.toString()
		$scope.completeCreateJob(post_data)
    }
        

   }
   //FUNCTION SUED TO HANDLE ADDRESS CHANGES
   $scope.changeAddress = function(address) {
       $scope.address= address
       if(address.country === "CA"){
       $scope.showCanadaRegions = true
       $scope.showAmericanRegions = false
       }else if (address.country === "US"){
        $scope.showCanadaRegions = false
       $scope.showAmericanRegions = true
 
       }
   	
   }
    //FUNCTION USED TO CHANGE THE VALUES OF GUARANTEE PAY
   // If true, then show guarantee pay
    $scope.changeGuaranteePay= function(val){
	$scope.useGuaranteePay= val
	//if guarantee pay is true then we must charge with card
	$scope.showPayWithCredit = val
    }

    //Function used to get the value
    $scope.changePay = function(val) {
	$scope.guaranteePayWage = val
    	
    }
    $scope.payWithCredit = function(card) {
	var haveError = false
	var errorMessage = "Oops We have a problem!"
	//check to make sure all fields are filled
	if(Stripe.card.validateCardNumber(card.creditCardNumber)===false ){
	data = "You must enter a valid credit card"	
	haveError = true
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		   });
		  return false;

	    }
	if( typeof card.creditCardExpiryDate === "undefined" ){
	 data = "You must enter a valid expiry date"	
	haveError = true
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false;
		   });
		  return false;


	}

	expiryMonth = card.creditCardExpiryDate.split("/")[0]
	expiryYear = card.creditCardExpiryDate.split("/")[1]
	//validate expiry month
	if(Stripe.card.validateExpiry(expiryMonth, expiryYear)=== false){
	 data = "You must enter a valid expiry date"	
	haveError = true
	var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false;
		   });
		  return false;

	    }
	if(Stripe.card.validateCVC(card.creditCardCVC) === false){
	    data = "You must enter a valid CVC"	
	    haveError = true
	    var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false;
		   });
		  return false;

	}
	if(haveError === true){
	    return false
	}	

	Stripe.card.createToken({
	  number: card.creditCardNumber,
	  cvc: card.creditCardCVC,
	  exp_month: expiryMonth,
	  exp_year: expiryYear
	},function(status,response){
	//if error from stripe
	if(response.error){
	     data = response.error.message	
	    haveError = true
	    var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
		  return false;
		   });
   
	}else{

	$scope.popover.hide()
	$scope.stripeToken = response.id
	$scope.createJob()
	}


	})
	
    
    }



    	

$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
      $scope.popover = popover;
    });
    //check guarantee pay wage before we show credit card form
    $scope.checkGuaranteePayValue = function($event) {
    	if($scope.guaranteePayWage === 0 || $scope.guaranteePayWage === null){
	data = "You specify a wage that you are going to pay"	
	errorMessage = "Oops looks like an error has occured"
	    haveError = true
	    var alertPopup = $ionicPopup.alert({
	        title: errorMessage,
	        template: data
	      }).then(function(res) {
	      	$scope.popover.hide()
		  return false;

		   });


	    return false
	
	}else{



	}
    }

   //FUNCTION used to check guarantee pay
    $scope.checkGuaranteePay = function() {
    if($scope.guaranteePayWage < 0) 	{
    $scope.allowGuaranteePay = false
    }
    else{
    $scope.allowGuaranteePay = true
    }
    }

   //FUNCTION used to set use address to true or false   
   $scope.useAddress= function(val){
    if(val === true){
      $scope.useJobAddress =true
   }else{
   $scope.useJobAddress = false
   }
   }
})
//*************************************************************************************
//CONTROLLER USED TO HANDLE ALL OF EMPLOYER SWIPING
.controller('employerSwipeCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,matchDataFactory,$rootScope, additionalInformationDataFactory,$ionicPopover,deleteJobService,$ionicPopup,deleteJobService,$timeout,newNotificationService){
  $scope.job = {}
  //get the current employee
  $scope.currentEmployee = {}
  $scope.employees=[]
   $scope.employeesType = []
  $scope.showNoEmployees = false
  //variables for message page show
  $scope.showMatchPage = false;
  $scope.matchData = {employee:{firstName:"dennis"}}
  //additional information upon request
  $scope.additionalInformation ={}
  $scope.rightTextOpacity ={}
  $scope.leftTextOpacity = {}
  $scope.lastEmployeeID = null
  $scope.showLoading = false
  //INIT FUNCTION
  $scope.getSwipedUsersInit = function(){
      $scope.showLoading = true
    query_params ={}
    query_params.socialUserID = RootSettings.userDetails.userID
    query_params.accessToken = RootSettings.userDetails.accessToken
    query_params.jobID= $stateParams.id
    //get job details
    $http.get(RootSettings.url+ '/jobs/getjobinfo',{params:query_params}).then(function(data){
      $scope.job = data.data
    })
    //get users that have swiped right
    $http.get(RootSettings.url + '/jobs/getemployeesswipedright',{params:query_params}).then(function(data){
     //no employees swiped right
       if(data.data.length === 0 ){
        $scope.showNoEmployees = true
	$scope.showLoading = false
         return
       } 
       //get all the employees
       
    $scope.employees = data.data

    //set current employee
    $scope.currentEmployee = $scope.employees[0]
    $scope.showLoading = false
    })

  }
  
    //FUNCTION on partial swipe
    $scope.cardPartialSwipe= function(amt){
       $scope.employees[0].rightTextOpacity = {
                  'opacity': amt > 0 ? 1 : 0
                };
     $scope.employees[0].leftTextOpacity = {
                  'opacity': amt < 0 ? 1 : 0
                };
    }
 
 //FUNCTION Go to employee profile
 $scope.getEmployeeProfile = function(id){
     $state.go('employee.viewProfile', {id:id})
     
     }
  $scope.addCard = function() {
    var newCard = $scope.employees[Math.floor(Math.random() * $scope.employees.length)];
  }
//FUNCTION that deals with swiping right of employee
  $scope.onTransitionRight = function(employeeID){
      $scope.swipeHandler(employeeID,'right')
      }

//FUNCTION used to deal with swipe left
$scope.onTransitionLeft = function(employeeID){
    $scope.swipeHandler(employeeID ,'left')
    
}
$scope.onTransitionOut= function(amt,employeeID){
               if (amt < 0) {
	                $scope.onTransitionLeft(employeeID);
	           } else {
	                    $scope.onTransitionRight(employeeID);
	                  }
    }

  //FUNCTION that deals with swiping right of employee
  $scope.onSwipeRight = function(employee_id){
      $scope.swipeHandler(employee_id,'right')
      }

//FUNCTION used to deal with swipe left
$scope.onSwipeLeft = function(employee_id){
    $scope.swipeHandler(employee_id,'left')
    
}
//FUNCTION for button swipe right, when user clicks buton
$scope.btnSwipeRight = function(){
    $scope.employees[0].rightTextOpacity={"opacity":1}
    //swipe right
    setTimeout(function(){
    $scope.employees[0].bounceOutRight=true
    $scope.swipeHandler($scope.currentEmployee.id, "right")
    },300)
    }
//FUNCTION used for button to simulate swipe left
$scope.btnSwipeLeft = function(){
    $scope.employees[0].leftTextOpacity = {"opacity":1}
    //swipe left
    //
    setTimeout(function(){
    $scope.employees[0].bounceOutLeft=true
    $scope.swipeHandler($scope.currentEmployee.id,'left')
    },300)
    }
//FUNCTION used to handle user swipe
$scope.swipeHandler=function(employee_id,direction){
    //Weird bug where if you swipe fast, swipe handler gets called twice
    if (employee_id === $scope.lastEmployeeID){
	return
    }else{
    $scope.lastEmployeeID = employee_id
    }

//remove card from stack
    //set up post data for 
    if($scope.employees.length ===0 ){
    return false
    }
    post_data = {}
    post_data.socialUserID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    post_data.jobID = $scope.job.id
    post_data.employeeID = employee_id
    post_data.selection = direction
    //selection for card
    $http.post(RootSettings.url + '/jobs/employerselection',post_data)
      .success(function(data){
        //check if match is made, if it is, then return the message page
	
	//we need to emit and remove a new applicant from front page
	$rootScope.$emit('employerSwiped', {job_id : $scope.job.id})
	//theres been a match
        if(data != "no match"){
            $scope.showMatchPage = true
            matchDataFactory.matchData = data
	    newNotificationService.setNewMatches(true)
	    newNotificationService.setNewMessages(true)
            $rootScope.$emit('matchUpdated')
           } 
        //check to see if stack is empty, if it is, then we will show that there are no employees left
	$scope.employees.shift()
        if($scope.employees.length === 0){
            $scope.showNoEmployees = true
	    
	    $scope.currentEmployee = {}
            }
	    else{
	    $scope.addCard()

        //reset new current employeee
	    $scope.currentEmployee = $scope.employees[0]
	    }
	    
    }).error(function(data){
        console.log(data)
      })
}
    //additional information shower
    $scope.showAdditionalInformation = function(type){
       $scope.additionalInformation.type = type
        //check to see what data we are returning
        if(type === "skills"){
            $scope.additionalInformation.data = $scope.currentEmployee.employeeSkills
            }
        else if (type == "education"){
            $scope.additionalInformation.data = $scope.currentEmployee.employeeEducation
            }    
        else if (type == "work experience"){
            $scope.additionalInformation.data = $scope.currentEmployee.employeeWorkExperience
            }    
        //Set data factory to new additional information
         additionalInformationDataFactory.additionalInformation = $scope.additionalInformation   
         $rootScope.$broadcast('additionalInformationUpdated')   
        }
//scope used to direct to edit job
$scope.editJob = function(id){
    //hide popover
	$scope.popover.hide()
	$state.go('jobs.editJob',{id:id})
   }
//show delete job \confirmation
$scope.deleteConfirm = function(job_id) {
   var confirmPopup = $ionicPopup.confirm({
        title: 'Delete This Job? ',
        template: 'Deleting this job will also remove all your matches'
      });
   confirmPopup.then(function(res) {
        if(res) {
		deleteJobService.deleteJob(job_id)
	     } else {
	          }
      });
}
//scpe userd to delete job
$scope.showDelete = function(id){
	$scope.popover.hide()
	$scope.deleteConfirm(id)
}
//handle popover
$ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
  }).then(function(popover) {
      $scope.popover = popover;
    });

//hide match page when button is clicked
$rootScope.$on('hideMatch',function() {
    $scope.showMatchPage = false;
})
})

//*************************************************************************
//CONTROLLER  USED TO EDIT EMPLOYER PROFILE

.controller('employerProfileCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,distanceService, profileDataFactory,RootSettings,$rootScope,$ionicHistory){
    $scope.userProfile = {}
    //check to see if it is user so we can edit profile
   $scope.isUser= false 
   $scope.saveSuccess = false
   $scope.editUserProfile ={}
    //Function used to initialize
    $scope.init = function(){
	id  = $stateParams.id
	//get e<Plug>PeepOpenloyer data 
	$http.get(RootSettings.url + "/userinfo/getemployerprofiledata?id=" + id).success(function(data){
	    console.log(data)
	    $scope.userProfile = data
	    	    	    console.log(id)
	    console.log(RootSettings.userDetails.id)
	    id = parseInt(id)
	    compareid = parseInt(RootSettings.userDetails.id)
	    //if is user, then we must show the edit profile button and if not we need to get distance
	    if(id === compareid){
		$scope.isUser = true
		}else{
	    employerLat = parseFloat($scope.userProfile.lastLatitude)
	    employerLong = parseFloat($scope.userProfile.lastLongitude)
	    myLat = RootSettings.currentLatitude
	    myLong = RootSettings.currentLongitude
	    distance = distanceService.getDistance(myLat,myLong,employerLat, employerLong)
	    $scope.userProfile.distance = distance
	    }
	    });
	}


	//init for editing employer profile
	$scope.editInit = function() {
	    id = $stateParams.id
	    query_params = {}
	    query_params.userID = RootSettings.userDetails.userID
	    query_params.accessToken = RootSettings.userDetails.accessToken
	    $http.get(RootSettings.url + '/userinfo/editemployerprofile', {params:query_params}).success(function(data){
		$scope.editUserProfile = data
		})
	}
    // on button click save profile
	$scope.saveProfile = function() {
	   post_data = {}
	    post_data.userID = RootSettings.userDetails.userID
	    post_data.accessToken = RootSettings.userDetails.accessToken
	    post_data.description = $scope.editUserProfile.description
	    $http.post(RootSettings.url + '/userinfo/editemployerprofile', post_data).success(function(data){
		$scope.showSaveSuccess = true
		//update description
		setTimeout(function(){$scope.showSaveSuccess = false},200)
		})

		$rootScope.$emit('updateDescription',{description:$scope.editUserProfile.description})
		
	}

    $rootScope.$on('updateDescription', function(event,args) {
description = args.description
		$scope.userProfile.description = description
		})
	//go to emplyoer dash
	
	$scope.profileBack =function() {
    if(RootSettings.userDetails.userType =="employer"){
      $state.go('employer.employerDashboard')
    }else{
    $ionicHistory.goBack()

    }
	}
	//go back to profile state
	$scope.editBack = function() {
	    $state.go('employer.employerDashboard')
	    
	}


	$scope.$on('profileUpdated',function() {
	    $scope.init()
	    $scope.editInit()
	$scope.userProfile.profilePictureUrl = profileDataFactory.profile.profilePictureUrl
	})

})
//******************************************************
//CONTROLLER used to control all functionality with matches
.controller('matchCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings, matchDataFactory,$rootScope,newNotificationService){
    $scope.matchData= matchDataFactory.matchData
    $scope.employee = {}
    $scope.employer = {}
    $scope.match= {}
    $scope.job = {}
    $scope.matchID
    $scope.matchInit = function(){
    }
    //goto chatroom
    $scope.gotoChatroom = function(id){
        $rootScope.$emit('hideMatch')
	newNotificationService.setNewMessages(false)
	$state.go('messages.chatRoom', {id:id})
	}
    //hide match value
    $scope.hideMatch= function(){
        $rootScope.$emit('hideMatch')
	}	

    //get broadcast to change match data
    $rootScope.$on('matchUpdated',function(){
        $scope.matchData = matchDataFactory.matchData
        $scope.employer = $scope.matchData.employer
        $scope.employee = $scope.matchData.employee
        $scope.job= $scope.matchData.job
	$scope.matchID = $scope.matchData.jobMatchID
	//need to create match chatroomid with QB
	var occupantID = $scope.employee.quickbloxID + "," + $scope.employer.quickbloxID
	console.log(occupantID)
	QB.chat.dialog.create({type: 3, occupants_ids:occupantID , name: $scope.matchData.jobMatchID},function(err,res){
	   console.log(res) 
	    //create the quickblox chatroom id
	    post_data= {}
            post_data.socialUserID = RootSettings.userDetails.userID
            post_data.accessToken = RootSettings.userDetails.accessToken
	    post_data.userType = RootSettings.userDetails.userType
	    console.log(post_data)
	    console.log(RootSettings.userDetails)
	    post_data.qbChatoomID= res._id
	    post_data.matchID = $scope.matchID
            $http.post(RootSettings.url + '/messages/setchatroom',post_data).success(function(data){
		console.log(data)
		})
		//update the matches
		   $rootScope.$emit('updateMatches')
 
	    })
        })


})

//*********************************************************
//CONTROLLER used to handle additional information panels
.controller('additionalInformationCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings, additionalInformationDataFactory){
    $scope.showAdditional=false
    $scope.showSkills =false
    $scope.showEducation = false;
    $scope.showWorkExperience = false;
    $scope.additionalInformation = {}

    //FUNCTION used to hide additional information
    $scope.hideAdditionalInformation = function(){
        $scope.showAdditional= false;
        $scope.additionalInformation = {}
        }
    //UPDATE additional information
    $scope.$on('additionalInformationUpdated', function(){
       $scope.additionalInformation= additionalInformationDataFactory.additionalInformation
       //check to see which additional information is being looked at
       if($scope.additionalInformation.type === "skills"){
           $scope.showSkills = true
          } 
      else if ($scope.additionalInformation.type === "education"){
          $scope.showEducation = true
          }     
       else if ($scope.additionalInformation.type === "work experience"){
           $scope.showWorkExperience = true
           } 
          $scope.showAdditional= true 
        })
})


//*********************************************************
//CONTROLLER used to handle with employee job search
.controller('employeeSwipeCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,quickbloxService,$ionicSideMenuDelegate, $timeout,$rootScope,newNotificationService,$interval,distanceService,$ionicPopup ){
$scope.jobs =[]
$scope.showNoJobs = false;
$scope.showLoading = false
//function used to get all the jobs for employee to swipe to
$scope.employeeSearchInit = function(){
    $scope.jobsAppeared=[]
    $scope.jobs =[]
    $scope.showLoading = true
    $scope.lastJobID = null
//make side menu unswipeable
$ionicSideMenuDelegate.canDragContent(false)
 quickbloxService.connectToQB()
 //set usertype to employee
   RootSettings.userDetails.userType = "employee"
   //check to see if there are any new notifications
    $interval(function() {
	newNotificationService.checkNewMatches()
	newNotificationService.checkNewMessages()
    },60000)
    newNotificationService.checkNewMatches()
	newNotificationService.checkNewMessages()


//only get jobs on init if there are no job cards present
if ($scope.jobs.length === 0 ){
    // get jobs for employee
    navigator.geolocation.getCurrentPosition(function(position){
                RootSettings.currentLongitude=position.coords.longitude
                RootSettings.currentLatitude=position.coords.latitude
                console.log(position.coords.longitude)
                 query_params = {}
                 query_params.socialUserID = RootSettings.userDetails.userID
                 query_params.accessToken = RootSettings.userDetails.accessToken
                 query_params.currentLongitude= RootSettings.currentLongitude.toString()
                 query_params.currentLatitude = RootSettings.currentLatitude.toString()
		    //ajax get of all jobs in the area
                  $http.get(RootSettings.url+'/jobs/getjobsinarea',{params:query_params}).then(function(data){
		      console.log(data)
		     $scope.showLoading = false
		      if (data.data.length === 0 ){
			  $scope.showNoJobs = true
			  return
			  }
		     var myLat = RootSettings.currentLatitude
		    var myLong = RootSettings.currentLongitude
		    //check to see if user has email for scraped jobs
		     if(data.data[0].isScraped && RootSettings.userDetails.email ===""){
			    var confirmPopup = $ionicPopup.confirm({
			         title: 'We need your email ',
			         template: 'These Jobs are Scraped From Job Sites. In order to apply for these jobs we will need your email so that the employer can contact you'
			       });
			    confirmPopup.then(function(res) {
			         if(res) {
				     $state.go('employee.getEmail')
				      } else {

					    $scope.showNoJobs = true
					    $scope.jobs=[]
					    return
				           }
			       });
			    
			    }

	    for (var i = 0, l = data.data.length; i < l; i ++) {
			   				jobLat = parseFloat(data.data[i].latitude)
				jobLong =  parseFloat(data.data[i].longitude)
				distance = distanceService.getDistance(myLat, myLong, jobLat, jobLong)
				data.data[i].distance = distance
			    	$scope.jobs.push(data.data[i]);
			    	$scope.jobsAppeared.push(data.data[i]);
			    }

		     //hide loading
                 })
        },function(data){alert("We couldn't get your location")},{ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
    }
}

//FUNCTION: get more jobs
$scope.lookForJobs = function() {
	if($scope.jobs.length === 0){
	$scope.showLoading = true
	}	
 query_params = {}
                 query_params.socialUserID = RootSettings.userDetails.userID
                 query_params.accessToken = RootSettings.userDetails.accessToken
                 query_params.currentLongitude= RootSettings.currentLongitude.toString()
                 query_params.currentLatitude = RootSettings.currentLatitude.toString()

	$scope.showNoJobs = false
	  $http.get(RootSettings.url+'/jobs/getjobsinarea',{params:query_params}).then(function(data){
		      console.log(data)
		      if (data.data.length === 0 ){
			  $scope.showNoJobs = true
			  $scope.jobs=[]
			$scope.showLoading =false
			  return
			  }
			  else{
			      var myLat = RootSettings.currentLatitude
			      var myLong = RootSettings.currentLongitude
			      //we add jobs to end of stack
			      ////check to see if user has email for scraped jobs
		     if(data.data[0].isScraped && RootSettings.userDetails.email ===""){
			    var confirmPopup = $ionicPopup.confirm({
			         title: 'We need your email ',
			         template: 'These Jobs are Scraped From Job Sites. In order to apply for these jobs we will need your email so that the employer can contact you'
			       });
			    confirmPopup.then(function(res) {
			         if(res) {
				     $state.go('employee.getEmail')
				      } else {

					    $scope.showNoJobs = true
					    $scope.jobs=[]
					    return
				           }
			       });
			    
			    }

			    for (var i = 0, l = data.data.length; i < l; i ++) {
				item = data.data[i]
				var isInJobs = false
				for (var x = 0, z = $scope.jobsAppeared.length; x < z; x ++) {
				    if(item.id === $scope.jobsAppeared[x].id || item.id === $scope.lastJobID){
					var isInJobs = true
					break
				    }
				}
				if (isInJobs === false){
					jobLat = parseFloat(data.data[i].latitude)
					jobLong =  parseFloat(data.data[i].longitude)
					distance = distanceService.getDistance(myLat, myLong, jobLat, jobLong)
					data.data[i].distance = distance.split(".")[0]
				    	$scope.jobs.push(data.data[i]);
				    	//push to jobsAppeared so it doesnt occur twice
				    	$scope.jobsAppeared.push(data.data[i]);
				 }

			    }
			    $scope.showNoJobs= false
			  }

		$scope.showLoading =false
                 })

}


//FUNCTION on partial swipe
    $scope.cardPartialSwipe= function(amt){
       $scope.jobs[0].rightTextOpacity = {
                  'opacity': amt > 0 ? 1 : 0
                };
     $scope.jobs[0].leftTextOpacity = {
                  'opacity': amt < 0 ? 1 : 0
                };
 
        }
  
  $scope.addCard = function() {
    var newCard = $scope.jobs[Math.floor(Math.random() * $scope.jobs.length)];
  }

  //FUNCTION that deals with swiping right of employee
  $scope.onSwipeRight = function(jobsID){
      $scope.swipeHandler(jobsID,'right')
      }

//FUNCTION used to deal with swipe left
$scope.onSwipeLeft = function(jobsID){
    $scope.swipeHandler(jobsID,'left')
    
}
  //FUNCTION that deals with swiping right of employee
  $scope.onTransitionRight = function(jobsID){
      $scope.swipeHandler(jobsID,'right')
      }

//FUNCTION used to deal with swipe left
$scope.onTransitionLeft = function(jobsID){
    $scope.swipeHandler(jobsID,'left')
    
}
$scope.onTransitionOut= function(amt,jobsID){
               if (amt < 0) {
	                $scope.onTransitionLeft(jobsID);
	           } else {
	                    $scope.onTransitionRight(jobsID);
	                  }
    }

//FUNCTION for button swipe right, when user clicks buton
$scope.btnSwipeRight = function(){
$scope.jobs[0].rightTextOpacity={"opacity":1}
    //swipe right
    setTimeout(function(){
    $scope.jobs[0].bounceOutRight=true
    $scope.swipeHandler($scope.jobs[0].id, "right")
    },300)
 
    }
//FUNCTION used for button to simulate swipe left
$scope.btnSwipeLeft = function(){
$scope.jobs[0].leftTextOpacity={"opacity":1}
    //swipe right
    setTimeout(function(){
    $scope.jobs[0].bounceOutLeft=true
    $scope.swipeHandler($scope.jobs[0].id, "left")
    },300)
    }

//FUNCTION USED TO OPEN SCRAPED JOBS
$scope.openJobInBrowser= function(job_url) {
window.open(job_url, '_system', 'location=yes'); return false;

}


//FUNCTION used to handle user swipe
$scope.swipeHandler=function(jobID,direction){
    //weird bug if you swipe too fast, swipe and ontransition gets called...
if (jobID === $scope.lastJobID){
	return
    }else{
    $scope.lastJobID = jobID
    }
    //if there are less then 5 cards remaining, we need to get more cards
    if($scope.jobs.length < 5){
    $scope.lookForJobs()
    }

//remove card from stack
    //set up post data for 
    post_data = {}
    post_data.socialUserID = RootSettings.userDetails.userID
    post_data.accessToken = RootSettings.userDetails.accessToken
    post_data.jobID = jobID
    post_data.selection = direction
    console.log(post_data)
    //selection for card
    $http.post(RootSettings.url + '/jobs/employeeselection',post_data)
      .success(function(data){
        //check if match is made, if it is, then return the message page
        if(data != "no match"){
            $scope.showMatchPage = true
            matchDataFactory.matchData = data.data
            $rootScope.$emit('matchUpdated')
           } 
        //check to see if stack is empty, if it is, then we will show that there are no employees left
	$scope.jobs.shift()
        if($scope.jobs.length === 0){
            $scope.showNoJobs = true
	    $scope.currentJob={}
            }
	    else{
	    $scope.addCard()
	    
	    }
        //reset new current employeee
    }).error(function(data){
        console.log(data)
      })

}
    //FUNCTION used to get additional information about employer
    $scope.showAdditionalInformation = function(type){
       $scope.additionalInformation.type = type
        //check to see what data we are returning
        if(type === "skills"){
            $scope.additionalInformation.data = $scope.currentEmployee.employeeSkills
            }
        else if (type == "education"){
            $scope.additionalInformation.data = $scope.currentEmployee.employeeEducation
            }    
        else if (type == "work experience"){
            $scope.additionalInformation.data = $scope.currentEmployee.employeeWorkExperience
            }    
        //Set data factory to new additional information
         additionalInformationDataFactory.additionalInformation = $scope.additionalInformation   
         $rootScope.$broadcast('additionalInformationUpdated')   
        }

    //FUNCTION used to go to employer profile
$scope.gotoEmployeeSettings = function() {
	$state.go('employee.viewSettings')
}
//Function used to update jobs
$rootScope.$on('reloadJobs', function() {
    $scope.jobs=[]
$scope.lookForJobs()	
})

$rootScope.$on('newMessage',function() {
	if($state.current.name!='messages.messagesDashboard' ){
                 newNotificationService.setNewMessages(true)
                     }
});
// open up all the job 1atches
  $scope.openJobMatches = function(){
      newNotificationService.setNewMatches(false)
      $rootScope.$emit('refreshMatches')
    $state.go('employee.getJobMatches')
  }
})
//****************************************************************************************
//CONTROLLER USED TO HANDLE EMPLOYEE PROFILE
.controller('employeeProfileCtrl',function($scope,distanceService,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,quickbloxService, additionalInformationDataFactory,$rootScope, profileDataFactory,$ionicPopup,$ionicHistory){
    $scope.userProfile = {}
    $scope.isUser = false
    $scope.additionalInformation = {}
//variables for adding items to profile
    $scope.newWorkExperience= {}
    $scope.newSkill =""
    $scope.newEducation = {}
    //variable for address
    $scope.address ={}
    //show dropdown stuff
    $scope.showAmericanRegions = false
    $scope.showCanadaRegions = false
    //init for view profile data
    $scope.viewProfileInit = function() {
	id = $stateParams.id
	$http.get(RootSettings.url + "/userinfo/getemployeeprofiledata?id=" +id).success(function(data){
	    $scope.userProfile = data
	    compareId = parseInt(RootSettings.userDetails.id)
	    id = parseInt(id)
	    //check to see if it is user
	    if(id === compareId){
		$scope.isUser = true
	    }
	    else{
		myLat = RootSettings.currentLatitude
		myLong = RootSettings.currentLongitude
		userLat = parseFloat($scope.userProfile.lastLatitude)
		userLong= parseFloat($scope.userProfile.lastLongitude)
		distance = distanceService.getDistance(myLat, myLong, userLat, userLong)
		$scope.userProfile.distance = distance
		}

	    })	
    }

    // FUNCTION show additional information
    $scope.showAdditionalInformation = function(type){
       $scope.additionalInformation.type = type
        //check to see what data we are returning
        if(type === "skills"){
            $scope.additionalInformation.data = $scope.userProfile.employeeSkills
            }
        else if (type == "education"){
            $scope.additionalInformation.data = $scope.userProfile.employeeEducation
            }    
        else if (type == "work experience"){
            $scope.additionalInformation.data = $scope.userProfile.employeeWorkExperience
            }    
//Set data factory to new additional information
         additionalInformationDataFactory.additionalInformation = $scope.additionalInformation   
         $rootScope.$broadcast('additionalInformationUpdated')   
        }
    //FUNCTION used to request cheque
    $scope.requestCheque = function() {
      if(RootSettings.userDetails.address.postalCode === null){
      var confirmPopup = $ionicPopup.confirm({
            title: 'You Are About to Accept A "Guarantee Pay" Job ',
            template: "In order to pay you for a guarantee pay job, we will need your address and postal code so that we can mail you the cheque "
          });
       confirmPopup.then(function(res) {
     //user is sure they want to assign job
            if(res) {
      $state.go('employee.updateAddress')
  }else{
      return
  }
})
     }
  else{
    	amountRequested = $scope.userProfile.amountMade
	if (amountRequested < 10){
	var alertPopup = $ionicPopup.alert({
	        title: 'You must have more then $10 to request a cheque',
	      }).then(function(res) {
      return false; 
	})
      }
      else{

	var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure?',
            template: "By clicking OK we will immediatly send you a cheque for the balance you have made through guarantee jobs"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
	post_data.userID = RootSettings.userDetails.userID
	post_data.accessToken = RootSettings.userDetails.accessToken
	$http.post(RootSettings.url + '/userinfo/requestcheque',post_data).success(function(data){
	var alertPopup = $ionicPopup.alert({
	        title: 'A cheque has been mailed to you',
	      }); 

	$scope.userProfile.amountMade = 0
	})

	}else{
	    return
	}

    })
    }}}
	//init top get address
    $scope.updateAddressInit = function() {
    	$scope.address= RootSettings.userDetails.address
		//if there is no address pressent then we assigned
	if($scope.address.city === null){
	    $scope.address.region = "BC"
	    $scope.address.country="CA"
	    $scope.showAmericanRegions = false
	    $scope.showCanadaRegions = true
	
	}
    }
    $scope.updateAddress= function(address) {
	if($scope.address.address === null || $scope.address.address === "" || $scope.address.city === null || $scope.address.city === "" || $scope.address.postalCode === null || $scope.address.postalCode === ""){
	var alertPopup = $ionicPopup.alert({
	        title: 'No fields can be left blank',
	        template: data
	      }); 
	}

	post_data = {}
	post_data.socialUserID = RootSettings.userDetails.userID
	post_data.accessToken = RootSettings.userDetails.accessToken
	post_data.address = address
	$http.post(RootSettings.url + '/userinfo/updateaddress',post_data).success(function(data){
	    RootSettings.userDetails.address = $scope.address
	 var alertPopup = $ionicPopup.alert({
	      title: 'Your Address Has Been Updated',
	    });
	       alertPopup.then(function(res) {
	    $ionicHistory.goBack()
	          });
	})


    }
//FUNCTION: this needs to run when we change the address
    $scope.changeAddress = function(address) {
		$scope.address= address
       if(address.country === "CA"){
       $scope.showCanadaRegions = true
       $scope.showAmericanRegions = false
       }else if (address.country === "US"){
        $scope.showCanadaRegions = false
       $scope.showAmericanRegions = true
 
    }
}

        
	// FUNCTION: go to emplyoee dash
	$scope.profileBack =function() {
	    if(RootSettings.userDetails.userType === "employee"){
	    $state.go('employee.viewSettings')
	    }else{
	    
    $ionicHistory.goBack()
	    }
	}
       $rootScope.$on('employeeProfileUpdated',function(){
       $scope.viewProfileInit()
       })
	$scope.$on('profileUpdated',function() {
	$scope.userProfile.profilePictureUrl = profileDataFactory.profile.profilePictureUrl
	})


    })
//*************************************************************************
//CONTROLLER USED TO EDIT EMPLOYEEPROFILE
.controller('editEmployeeProfileCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings, chatroomJobDataFactory,matchMessagesFactory,$rootScope,$ionicPopup){
    $scope.userProfile = {}
    //FUNCTION check to see if we can edit user Profile
    $scope.editInit = function() {
	id = $stateParams.id
	    query_params = {}
	    query_params.userID = RootSettings.userDetails.userID
	    query_params.accessToken = RootSettings.userDetails.accessToken
	    $http.get(RootSettings.url + '/userinfo/editemployeeprofile', {params:query_params}).success(function(data){
		console.log(data)
		$scope.userProfile = data
		})

    	
    }
//FUNCTION: Update employee profile values
	$scope.updateEmployeeProfile = function(type) {
	    //post data
	    post_data = {}
	    post_data.userID = RootSettings.userDetails.userID
	    post_data.accessToken = RootSettings.userDetails.accessToken
	    post_data.typeCategory = type
	    //check what is being updatedi
	    if(type === "skill"){
		post_data.data = $scope.userProfile.newSkill	
	    }else if (type === "work experience"){
		post_data.data = $scope.userProfile.newWorkExperience
	    }else if(type  === 'education'){
		post_data.data = $scope.userProfile.newEducation
	    }else if(type === "description"){
		post_data.data = $scope.userProfile.description
	    }

	    //post to update
	    $http.post(RootSettings.url + '/userinfo/updateemployeeprofile',post_data).success(function(data){
	    if(type === "skill"){
		$scope.userProfile.employeeSkills.push(data)
		$scope.userProfile.newSkill =""
	    }else if (type === "work experience"){
		$scope.userProfile.employeeWorkExperience.push(data)
		$scope.userProfile.newWorkExperience = {}
	    }else if(type  === 'education'){
		$scope.userProfile.employeeEducation.push(data)
		$scope.userProfile.newEducation= {}
	    }else if(type === "description"){
	    }
	    $rootScope.$emit('employeeProfileUpdated')

	    
	    }).error(function(data){
		console.log(data)
	   var alertPopup = $ionicPopup.alert({
	        title: 'Oops Something Went Wrong',
	        template: data
	      }); 
	    })
	}
	//FUNCTION USED TO DELETE EMPLOYEE PROFILE STUFF
	$scope.deleteEmployeeProfile = function(type,id){
	     post_data = {}
	    post_data.userID = RootSettings.userDetails.userID
	    post_data.accessToken = RootSettings.userDetails.accessToken
	    post_data.typeCategory = type
	    //check what is being updatedi
	    if(type === "skill"){
		post_data.delete_id = id 
	    }else if (type === "work experience"){
		post_data.delete_id = id
	    }else if(type  === 'education'){
		post_data.delete_id = id
	    }

	    //post to update
	    $http.post(RootSettings.url + '/userinfo/deleteemployeeprofile',post_data).success(function(data){
	    if(type === "skill"){
		for (var i = 0, l = $scope.userProfile.employeeSkills.length; i < l; i ++) {
		    if($scope.userProfile.employeeSkills[i].id === id){
		    $scope.userProfile.employeeSkills.splice(i,1)
		    }
		}
	    }else if (type === "work experience"){
		for (var i = 0, l = $scope.userProfile.employeeWorkExperience.length; i < l; i ++) {
		    if($scope.userProfile.employeeWorkExperience[i].id === id){
		    $scope.userProfile.employeeWorkExperience.splice(i,1)
		    }
		}
	    }else if(type  === 'education'){
		for (var i = 0, l = $scope.userProfile.employeeEducation.length; i < l; i ++) {
		    if($scope.userProfile.employeeEducation[i].id === id){
		    $scope.userProfile.employeeEducation.splice(i,1)
		    }
		}

	    }
	    $rootScope.$emit('employeeProfileUpdated')

	    
	    }).error(function(data){
		console.log(data)
	   var alertPopup = $ionicPopup.alert({
	        title: 'Oops Something Went Wrong',
	        template: data
	      }); 
	    })

	}

	//FUNCTION: go back to profile state
	$scope.editBack = function() {
	    $state.go('employee.viewSettings')
	    
	}
})
//*****************************************************************************
//CONTROLLER USED TO HANDLE CHANGING DISCOVERY PREFERENCES

.controller('changeDiscoveryPreferencesCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings, chatroomJobDataFactory,matchMessagesFactory,$rootScope){
//jobtypes
$scope.selectedJobTypes= []
$scope.jobTypes = []
//search distamce
$scope.searchDistance = 0
$scope.lowestAcceptableWage= 0


$scope.init = function(){
    //get job types
     $http.get(RootSettings.url +'/jobs/getjobclasses')
                    .then(function(data){
                      $scope.jobTypes= data.data
                      console.log($scope.jobTypes)
                    })

    params = {}
    params.accessToken = RootSettings.userDetails.accessToken
    params.userID = RootSettings.userDetails.userID
     $http.get(RootSettings.url+'/userinfo/getuserdiscoverypreferences',{params:params}).then(function(data){
	 
    $scope.selectedJobTypes =  data.data.selectedJobTypes
    $scope.lowestAcceptableWage = data.data.lowestAcceptableWage
    $scope.searchDistance = data.data.searchDistance
    //check the different job types if already checked, then check box
    for (var i = 0, l = $scope.jobTypes.length; i < l; i ++) {
	for (var s = 0, x = $scope.selectedJobTypes.length; s < x; s ++) {
	if($scope.jobTypes[i].id === $scope.selectedJobTypes[s]){
	    $scope.jobTypes[i].checked = true
	}
	}
    }
    console.log($scope.jobTypes)
     })
}

//function dealing with the addition of jobtype
    // //FUNCTION that deals with selecting job types user is interested in
  $scope.selectJobType = function(jobtype_id) {
      var idx = $scope.selectedJobTypes.indexOf(jobtype_id);

      // is currently selected
      if (idx > -1) {
        $scope.selectedJobTypes.splice(idx, 1);
      }

      // is newly selected
      else {
        $scope.selectedJobTypes.push(jobtype_id);
      }
      console.log($scope.selectedJobTypes)
    };


    //Function to update discovery preferences
    $scope.updateDiscoverySettings = function() {
    		post_data = {}
	post_data.userID = RootSettings.userDetails.userID
	post_data.accessToken = RootSettings.userDetails.accessToken
	post_data.searchDistance = $scope.searchDistance
	post_data.lowestAcceptableWage = $scope.lowestAcceptableWage
	post_data.selectedJobTypes = $scope.selectedJobTypes
	console.log(post_data)
    //get details for chatroom
      $http.post(RootSettings.url + '/userinfo/updateuserdiscoverypreferences',post_data).success(function(data){
	  $rootScope.$emit('reloadJobs')
	  $state.go('employee.searchJobs')
	  
    })
    }

    $scope.updateWage= function(newwage) {
	$scope.lowestAcceptableWage = newwage
    	
    }
    $scope.updateDistance = function(newdistance) {
    	$scope.searchDistance= newdistance
    }

})
//****************************************************************************
//CONTROLLER USED TO HANDLE MESSAGES
.controller('messagesCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings, chatroomJobDataFactory,matchMessagesFactory,$rootScope,$ionicPopup){
    $scope.messages = []
    $scope.chatroomJobs = []
    $scope.showLoading = true
    //INIT FUNCTION
    $scope.init = function(){
	$scope.messages=[]
   query_params = {}
    query_params.socialUserID = RootSettings.userDetails.userID
    query_params.userType = RootSettings.userDetails.userType
    query_params.accessToken = RootSettings.userDetails.accessToken
    //show loading spinner
    $scope.showLoading = true
     $http.get(RootSettings.url+'/messages/getmatchmessages',{params:query_params}).then(function(data){
	 for(i = 0 ; i  < data.data.length; i++){
	     console.log(data.data[i].lastMessageSent)
	     //no message as been sent yet, so show match date
	     if(data.data[i].lastMessageText == ""){
		data.data[i].showMessage = "Matched: " + moment(data.data[i].lastMessageSent).format("MMM D")
		console.log(data.data[i])
		 }
		 else{
		     data.data[i].showMessage = data.data[i].lastMessageText 
		     }
	     }
	 $scope.messages= data.data
	 matchMessagesFactory.matchMessages = $scope.messages
	 $scope.showLoading = false
	
	})
    }

$scope.refreshMessages= function() {
query_params = {}
    query_params.socialUserID = RootSettings.userDetails.userID
    query_params.userType = RootSettings.userDetails.userType
    query_params.accessToken = RootSettings.userDetails.accessToken
    //show loading spinner
     $http.get(RootSettings.url+'/messages/getmatchmessages',{params:query_params}).then(function(data){
	 for(i = 0 ; i  < data.data.length; i++){
	     console.log(data.data[i].lastMessageSent)
	     //no message as been sent yet, so show match date
	     if(data.data[i].lastMessageText == ""){
		data.data[i].showMessage = "Matched: " + moment(data.data[i].lastMessageSent).format("MMM D")
		console.log(data.data[i])
		 }
		 else{
		     data.data[i].showMessage = data.data[i].lastMessageText 
		     }
	     }
	 $scope.messages= data.data
	 matchMessagesFactory.matchMessages = $scope.messages
	
       $scope.$broadcast('scroll.refreshComplete');
})
}

    //refresh to get all jobs
      $scope.refreshChatroomJobs = function(){
       params ={}
       params.userID = RootSettings.userDetails.userID
       params.accessToken = RootSettings.userDetails.accessToken
       params.otherUserID = chatroomJobDataFactory.otherUserID
       params.userType = RootSettings.userDetails.userType
       $http.get(RootSettings.url + '/messages/getjobschatroom',{params:params}).success(function(data) {
     console.log(data)
     jobs=  data
      for (var i = 0, l = jobs.length; i < l; i ++) {
      jobs[i].dateApplied =moment(jobs[i].dateApplied).format("MMM D")
      }
     chatroomJobDataFactory.chatroomJobs = jobs
     $rootScope.$broadcast('chatroomJobsUpdated')
       $scope.$broadcast('scroll.refreshComplete');
          $scope.$apply()

       })

       }
//FUNCTUON USED TO ACCEPT JOB
    $scope.acceptOffer = function(matchID,userType){
    if (matchType === "guarantee" && RootSettings.userDetails.address.postalCode === null){
	//check to see if there is a postal code so we can mail the check
	if(RootSettings.userDetails.address.postalCode === null){
	    var confirmPopup = $ionicPopup.confirm({
            title: 'You Are About to Accept A "Guarantee Pay" Job ',
            template: "In order to pay you for a guarantee pay job, we will need your address and postal code so that we can mail you the cheque "
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
	    $state.go('employee.updateAddress')
	}else{
	    return
	}
     })
	}
     }

    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Are you sure you want to accept this job? Once you accept this job you cannot unmatch"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		$http.post(RootSettings.url + "/jobs/acceptoffer", post_data).success(function(data){
		    //change matchdata to assigned

			for (var i = 0, l = chatroomJobDataFactory.chatroomJobs.length; i < l; i ++) {
			    if(matchID == chatroomJobDataFactory.chatroomJobs[i].matchID){
				//make job that was assigned, assigned
			     chatroomJobDataFactory.chatroomJobs[i].isHired = true
			    }
			}
			//reset the matches
			$scope.chatroomJobs = chatroomJobDataFactory.chatroomJobs
		
		}).error(function(data){
		    console.log(data)
		})
		//USER clicked cancel
	         } else {
		     return false
		      }
          });
    //prepare post data
    
   
    }

//FUNCTUON USED TO ACCEPT JOB
    $scope.jobCompleted = function(matchID){
    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Once this job is marked as completed, it cannot be undone"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		$http.post(RootSettings.url + "/jobs/completejob", post_data).success(function(data){
		    //change matchdata to assigned

			for (var i = 0, l = chatroomJobDataFactory.chatroomJobs.length; i < l; i ++) {
			    if(matchID == chatroomJobDataFactory.chatroomJobs[i].matchID){
				//make job that was assigned, assigned
			     chatroomJobDataFactory.chatroomJobs[i].isCompleted =true
			    }
			}
			//reset the matches
			$scope.chatroomJobs = chatroomJobDataFactory.chatroomJobs
		
		}).error(function(data){
		    console.log(data)
		})
		//USER clicked cancel
	         } else {
		     return false
		      }
          });
    //prepare post data
    
   
    }
//ASIGN JOB TO USER
    $scope.removeMatch = function(matchID){
    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Are you sure you want to remove this match"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		post_data.userType = RootSettings.userDetails.userType
		$http.post(RootSettings.url + "/jobs/removematch", post_data).success(function(data){
		    //change matchdata to 
		    var otherUser=null
			for (var i = 0, l = chatroomJobDataFactory.chatroomJobs.length; i < l; i ++) {
			    console.log(chatroomJobDataFactory.chatroomJobs[i].matchID)
			    if(matchID === chatroomJobDataFactory.chatroomJobs[i].matchID){
			    //remove user from messages list
			    otherUser = chatroomJobDataFactory.chatroomJobs[i].otherUser.id
			     chatroomJobDataFactory.chatroomJobs.splice(i,1)
			    }
			}
			
			//reset the matches
			$scope.chatroomJobs = chatroomJobDataFactory.chatroomJobs
			//if there are no jobs, then return to message dash
			if ($scope.chatroomJobs.length ===0 ){
			    //loops through all the messages and remove message from message list
			    for (var i = 0, l = $scope.messages.length; i < l; i ++) {
				if ($scope.messages[i].user.id === otherUser){
				    $scope.messages.splice(i,1)
				    $scope.$apply()
				    break
				}
			}
				$state.go('messages.messagesDashboard')
			}
		
		}).error(function(data){
		    console.log(data)
		})
		//USER clicked cancel
	         } else {
		     return false
		      }
          });
    //prepare post data
    
   
    }


//ASIGN JOB TO USER
    $scope.offerMatch = function(matchID){
    //create confirm box to make sure user wants to assign job
    var confirmPopup = $ionicPopup.confirm({
            title: 'Are you sure? ',
            template: "Once you offer this job to a user, you cannot pull back your offer. Once they accept this offer, your job will be pulled from the job pool"
          });
       confirmPopup.then(function(res) {
	   //user is sure they want to assign job
            if(res) {
		 post_data = {}
		 post_data.userID = RootSettings.userDetails.userID
		 post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.matchID = matchID
		$http.post(RootSettings.url + "/jobs/offermatch", post_data).success(function(data){
		    //change matchdata to assigned

			for (var i = 0, l = chatroomJobDataFactory.chatroomJobs.length; i < l; i ++) {
			    if(matchID == chatroomJobDataFactory.chatroomJobs[i].matchID){
				//make job that was assigned, assigned
			     chatroomJobDataFactory.chatroomJobs[i].isOffered = true
			    }
			}
			//reset the matches
			$scope.chatroomJobs = chatroomJobDataFactory.chatroomJobs
		
		}).error(function(data){
		    console.log(data)
		})
		//USER clicked cancel
	         } else {
		     return false
		      }
          });
    //prepare post data
    
   
    }

    //CHANGE TO MESSAGE SYSTEM
    $scope.getChatRoom = function(id){
	console.log(id)
         $state.go('messages.chatRoom',{'id':id});
	}

	// when new message received
	$rootScope.$on('newMessage',function() {
	    $scope.messages = matchMessagesFactory.matchMessages
	    $scope.$apply()
	})
//when you get a new match
    $rootScope.$on('updateMatches',function(){
	//run init to get all the matches again
   $scope.init() 
    })

    $rootScope.$on('newMessage', function() {
    	    $scope.messages = matchMessagesFactory.matchMessages

    })
	//when user sents new message
    //
    $rootScope.$on('messageSent',function(){
		    $scope.messages = matchMessagesFactory.matchMessages
	})
	// when message gets marked as read
	$rootScope.$on('messageMarkedAsRead', function(){
	    console.log(matchMessagesFactory.matchMessages)
	    $scope.messages = matchMessagesFactory.matchMessages
	    })
	$scope.$on('chatroomJobsUpdated',function() {
	    $scope.chatroomJobs =  chatroomJobDataFactory.chatroomJobs
	    // $scope.$apply()
	})
//refresh chat messages
       $rootScope.$on('refreshMessages', function() {
       	$scope.init()
       })



})

//**********************************************************************************
//CONTROLLER used to control the chatrooms
//
//
.controller('chatroomCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,newMessageFactory,$rootScope,$ionicScrollDelegate,chatroomJobDataFactory,matchMessagesFactory,newNotificationService){
    $scope.chatroomID = ""
    $scope.chatroomDetails = {}
    $scope.chatHistory=[]
    $scope.newMessage = ""
    $scope.showLoading = false
    $scope.lastMessageID =""
   //INIT Function` 
    $scope.init = function(){
	$scope.showLoading = true
	$scope.chatroomID = $stateParams.id
	post_data = {}
	post_data.socialUserID = RootSettings.userDetails.userID
	post_data.accessToken = RootSettings.userDetails.accessToken
	post_data.chatroomID = $scope.chatroomID
	post_data.userType = RootSettings.userDetails.userType
    //get details for chatroom
      $http.get(RootSettings.url + '/messages/getchatroom',{params:post_data}).success(function(data){
	  console.log(data)
//wget all chatroom details
    $scope.chatroomDetails = data
    // get the messaage list
    var req = {
	 method: 'GET',
	 url: 'https://api.quickblox.com/chat/Message.json?chat_dialog_id='+$scope.chatroomDetails.chatroomID,
	 headers: {
	     'QB-Token':RootSettings.QBToken
		    },
		    }
	    	
    $http(req).success(function(res){
		for(var i = 0 ; i < res.items.length ; i ++){
		    $scope.chatHistory.push(res.items[i])
		}
	//	$scope.chatHistory = res.items
		//iterate through the chathistory and see which messages were sent by user
		for(var i = 0 ; i < $scope.chatHistory.length ; i ++){
		    //if other user sent message
		    if($scope.chatHistory[i].sender_id === $scope.chatroomDetails.user.quickbloxID){
		$scope.chatHistory[i].sentByMe= false	
			}
		    else{
			$scope.chatHistory[i].sentByMe = true	    
			    }
		    
		    }
		    console.log($scope.chatHistory)
		    //mark as read
		post_data = {}
		post_data.socialUserID = RootSettings.userDetails.userID
		post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.chatroomID = $scope.chatroomDetails.chatroomID
		post_data.userType = RootSettings.userDetails.userType
		post_data.matchID = $scope.chatroomDetails.matchID
		$http.post(RootSettings.url + '/messages/markasread',post_data).success(function(data){
for (var i = 0, l = matchMessagesFactory.matchMessages.length; i < l; i ++) {
		if(matchMessagesFactory.matchMessages[i].id === $scope.chatroomDetails.matchID){
	    matchMessagesFactory.matchMessages[i].isUnread = false
	    $rootScope.$emit('messageMarkedAsRead')
		}

	    }
		})

		  	    })
	    
	    // we need to update the match to change last 
	       
	})
  $scope.showLoading = false
		    $ionicScrollDelegate.scrollBottom(true)


}
 $scope.inputUp = function() {
    if (isIOS) $scope.data.keyboardHeight = 216;
    $timeout(function() {
      $ionicScrollDelegate.scrollBottom(true);
    }, 300);

  };

  $scope.inputDown = function() {
    if (isIOS) $scope.data.keyboardHeight = 0;
    $ionicScrollDelegate.resize();
  };


    // send new message to user
    $scope.sendMessage = function(){
	if($scope.newMessage === ""){
	    return
	     }
	    var jid = ($scope.chatroomDetails.user.quickbloxID).toString() + "-18291@chat.quickblox.com"
	    QB.chat.send(jid,{type:'chat',body:$scope.newMessage, extension:{
	    save_to_history:1
	     }})
        message = {}
        message.message = $scope.newMessage
        message.sentByMe= true
        $scope.chatHistory.push(message)
        $ionicScrollDelegate.scrollBottom(true)
        
  //   	 var req = {
	 // method: 'POST',
	 // url: 'https://api.quickblox.com/chat/Message.json',
	 // headers: {
	 //     'Content-Type':'application/json',
	 //     'QB-Token':RootSettings.QBToken
		//     },
	 //     data: { chat_dialog_id:$scope.chatroomDetails.chatroomID,message:$scope.newMessage,send_to_chat:1,save_to_history:1},
		//     }
		// $http(req)
	//we need to update match record to show there is  a last message
	post_data = {}
	post_data.socialUserID = RootSettings.userDetails.userID
	post_data.accessToken = RootSettings.userDetails.accessToken
	post_data.chatroomID = $scope.chatroomDetails.chatroomID
	post_data.userType = RootSettings.userDetails.userType
	post_data.matchID = $scope.chatroomDetails.matchID
	post_data.lastMessage = $scope.newMessage
        $http.post(RootSettings.url + '/messages/updatechatroom',post_data).success(function(data){
	    console.log('message sent')
	})
	//UPDATE MESSAGES
	for (var i = 0, l = matchMessagesFactory.matchMessages.length; i < l; i ++) {
		    if(matchMessagesFactory.matchMessages[i].dialogID === $scope.chatroomDetails.chatroomID){
			//change last message sent
			matchMessagesFactory.matchMessages[i].showMessage = $scope.newMessage
			match_message = matchMessagesFactory.matchMessages[i]
			//remove message from array and readd to top
			matchMessagesFactory.matchMessages.splice(i,1)
			matchMessagesFactory.matchMessages.unshift(match_message)
			}
		}
		$rootScope.$emit('messageSent')
	$scope.newMessage = ""
    }
//FUNCTION used to get all the chatroom jobs that have been applied for
   $scope.getChatroomJobs = function(){
       params ={}
       params.userID = RootSettings.userDetails.userID
       params.accessToken = RootSettings.userDetails.accessToken
       params.otherUserID = $scope.chatroomDetails.user.id
       params.userType = RootSettings.userDetails.userType
       $http.get(RootSettings.url + '/messages/getjobschatroom',{params:params}).success(function(data) {
	   console.log(data)
	   jobs=  data
	    for (var i = 0, l = jobs.length; i < l; i ++) {
	    jobs[i].dateApplied =moment(jobs[i].dateApplied).format("MMM D")
	    }
	   chatroomJobDataFactory.chatroomJobs = jobs
     chatroomJobDataFactory.otherUserID = $scope.chatroomDetails.user.id
	   $rootScope.$broadcast('chatroomJobsUpdated')

       })
       }

    //When we receive new message, we must add it to the user
    $rootScope.$on('newMessage', function(){

	newMessage =  newMessageFactory.newMessage
	if($scope.lastMessageID === newMessage.message.extension.message_id ){
		newNotificationService.setNewMessages(false)
		return
	}
	else{
		$scope.lastMessageID = newMessage.message.extension.message_id
	}
	addMessage={}
	addMessage.sender_id = newMessage.userID
	addMessage.message = newMessage.message.body
	addMessage.date_sent = newMessage.message.extension.date_sent
	addMessage._id = newMessage.message.extension.message_id
	    console.log($scope.chatroomDetails.user.quickbloxID)
	//check to see if new message is from chatroom
	    console.log($state.current.name)
      //if id is the same as sent user, we make usre that its marked as read
  if( $state.current.name === "messages.chatRoom" && RootSettings.userDetails.quickbloxID === newMessage.userID  ){
    newNotificationService.setNewMessages(false)
  }

	if (newMessage.userID === $scope.chatroomDetails.user.quickbloxID &&  $state.current.name === "messages.chatRoom"){

	    addMessage.sentByMe = false
	    console.log(addMessage)
	   $scope.chatHistory.push(addMessage) 
	// mark that there is no new message
	
	//mark as read
	post_data = {}
	post_data.socialUserID = RootSettings.userDetails.userID
	post_data.accessToken = RootSettings.userDetails.accessToken
	post_data.chatroomID = $scope.chatroomDetails.chatroomID
	post_data.userType = RootSettings.userDetails.userType
	post_data.matchID = $scope.chatroomDetails.matchID
        $http.post(RootSettings.url + '/messages/markasread',post_data).success(function(data){
	    for (var i = 0, l = matchMessagesFactory.matchMessages.length; i < l; i ++) {
		if(matchMessagesFactory.matchMessages[i].id === $scope.chatroomDetails.matchID){
	    matchMessagesFactory.matchMessages[i].isUnread = false
	    $rootScope.$emit('messageMarkedAsRead')
		}

	    }
	    console.log('message mark as read')
	})
	}
	$scope.$apply()
	$ionicScrollDelegate.scrollBottom(true)
	//mark as read
	
    })

    
})

.controller('introCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,newMessageFactory,$rootScope,$ionicScrollDelegate,chatroomJobDataFactory,matchMessagesFactory,newNotificationService){
 $scope.myActiveSlide = 2;
 $scope.gotoEmployeeDash = function() {
 	$state.go('employee.searchJobs')
 }
 $scope.gotoEmployerDash = function() {
 	$state.go('employer.employerDashboard')
 }
})

.controller('employeeGetEmailCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,newMessageFactory,$rootScope,$ionicScrollDelegate,$ionicPopover,$ionicPopup){
    $scope.email = ""
    $scope.showError = false
    $scope.errorMessage= ""
    $scope.init = function() {
    	$scope.email = ''
    }
    $scope.updateEmail = function(email) {
	$scope.showError = false
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	console.log(email)
	if( re.test(email) === false){
	    $scope.showError = true
	    $scope.errorMessage = "You need to enter a valid email"
	    return
	}
	    post_data= {}
                post_data.userID= RootSettings.userDetails.userID
                post_data.accessToken = RootSettings.userDetails.accessToken
		post_data.email= email
                $http.post(RootSettings.url + '/userinfo/setemail',post_data).success(function(data){
		    RootSettings.userDetails.email = email
		    $rootScope.$emit('reloadJobs')
		    $state.go('employee.searchJobs')
		})

	

    	
    }
})
//****************************************************************************************
//CONTROLLER USED TO MANAGE JOBS
//
.controller('jobsCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,newMessageFactory,$rootScope,$ionicScrollDelegate){
    })
.controller('jobProfileCtrl',function($scope,locationService,$state,$stateParams,$http, $ionicNavBarDelegate, RootSettings,newMessageFactory,$rootScope,$ionicScrollDelegate,$ionicPopover,$ionicPopup){
    //local variables
    $scope.jobDetails = {}
    $scope.isUser = false
        //FUNCTION init function for the job profile
    $scope.init = function () {
	job_id = $stateParams.id    	
	//get job data	
	$http.get(RootSettings.url + "/jobs/getjobdetails?id=" + job_id).success(function(data){
	    $scope.jobDetails = data
	    //if user posted job
	    if($scope.jobDetails.postedBy.id === RootSettings.userDetails.id){
		$scope.isUser = true
		console.log('isuser')
		}
	    })
    }
//Function used to edit job details     
   $scope.editJob = function(id){
	$state.go('jobs.editJob',{id:id})
   }
//show delete job \confirmation
$scope.deleteConfirm = function(job_id) {
   var confirmPopup = $ionicPopup.confirm({
        title: 'Delete This Job? ',
        template: 'Deleting this job will also remove all your matches'
      });
   confirmPopup.then(function(res) {
        if(res) {
		deleteJobService.deleteJob(job_id)
	     } else {
	          }
      });
}

$scope.showDelete = function(id){
	$scope.popover.hide()
	$scope.deleteConfirm(id)
}

    
    })
.controller('PlaylistCtrl', function($scope, $stateParams) {
});





