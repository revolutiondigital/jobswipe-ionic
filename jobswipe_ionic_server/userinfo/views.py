# Create your views here.
from decimal import *
import datetime
from django.conf import settings
from django.shortcuts import render_to_response, render,redirect
from django.template.defaulttags import csrf_token
from django.template import RequestContext
from django.contrib.auth.models import User,Group
from django.contrib.auth import *
from django.http import  *
from django.conf import settings
from django.middleware.csrf import get_token
import json
import ast
from userinfo.models import *
from django.views.decorators.http import *
from django.contrib.auth.decorators import login_required
import urllib2
from django.views.decorators.csrf import *
import requests
from boto.s3.connection import S3Connection
import cStringIO
from PIL import Image
import base64
#FUNCTION
#create user from facebook/linkedin
@csrf_exempt
@require_POST
def user_signup(request):
	#check to see if user is already created.. if they are, redirect to dashboard
	print request.body
	try:
		post_data= ast.literal_eval(request.body)
		facebook_data = post_data.get('facebookUserDetails')
		first_name = facebook_data.get('first_name')
		last_name = facebook_data.get('last_name')
		gender = facebook_data.get('gender')
		social_user_id = facebook_data.get('userID')
		timezone = facebook_data.get('timezone')
		locale = facebook_data.get('locale')
		profile_picture_url = facebook_data.get('profile_picture_url')
		access_token = facebook_data.get('accessToken')
		current_latitude = post_data.get('currentLatitude')
		current_longitude = post_data.get('currentLongitude')
	except Exception,e  :
		print e
		return HttpResponseBadRequest()
	print post_data
	current_latitude = Decimal(current_latitude)
	current_longitude = Decimal(current_longitude)
	#get long access token
	long_token = get_facebook_long_token(access_token)
	#check to see if user id exists in the db
	try:
		userinfo = UserInfo.objects.get(social_user_id = social_user_id)
		userinfo.last_latitude = current_latitude
		userinfo.last_longitude = current_longitude
		userinfo.access_token = long_token
		userinfo.save()
		#create data to return
		return_data = {}
		#new long access token
		return_data['access_token'] = long_token
		#new user_type
		if userinfo.is_employee:
			user_type = "employee"

		elif userinfo.is_employer:
			user_type = "employer"
		else:
			user_type = "none"
		return_data['user_type'] = user_type
		return_data['id'] = userinfo.id
		address= {}
		address['address'] = userinfo.address
		address['city'] = userinfo.city
		address['region'] = userinfo.region
		address['country'] = userinfo.country
		address['postalCode'] = userinfo.postal_code
		return_data['address']=  address
		return_data['email'] = userinfo.email
		print return_data
		return HttpResponse(json.dumps(return_data))
	except Exception,e:
		print e
		pass
	#if user doesn exist in the db,then we must go make a new one ahead
	try:
		new_user = UserInfo(first_name = first_name,
							last_name = last_name,
							profile_picture_url= profile_picture_url,
							sex= gender,
							last_latitude= current_latitude,
							last_longitude= current_longitude,
							is_facebook= True,
							is_linkedin= False,
							social_user_id= social_user_id,
							access_token= long_token,
							timezone= timezone)
		new_user.save()
	#if user is employee, then we need to also create all the job types that belong

	except Exception, e:
		print e
		return HttpResponseServerError()
	return_data={}
	return_data['access_token'] = long_token
	#new user_type
	return_data['user_type'] = "None"
	return_data['id'] = new_user.id
	address= {}
	address['address'] = new_user.address
	address['city'] = new_user.city
	address['region'] = new_user.region
	address['country'] = new_user.country
	address['postalCode'] = new_user.postal_code
	return_data['address'] = address
	return_data['email'] = email
	return HttpResponse(json.dumps(return_data))


#FUNCTION
#check to see if facebook token exists in our db
@require_GET
def check_facebook_token(request):
	try:
		short_token = request.GET.get('facebook_short_token')
		user_id = request.GET.get('socialUserID')
	except:
		return HttpResponseBadRequest()
	long_token = get_facebook_long_token(short_token)
	#check to see if the long token for that user id exists.
	try:
		user = UserInfo.objects.get(social_user_id = user_id)
		user.access_token = long_token
		user.save()
		return_data = {}
		return_data['access_token'] = long_token
		return_data['id']= user.id
		return HttpResponse(json.dumps(return_data))
	except Exception,e:
		print e
		return HttpResponseBadRequest()
		pass
	#check to see if user exists but fb token may have run out

#FUNCTION USED TO GET USER ROLE (i.e employer, employee)
#input: {socialUserID, accessToken}
@require_GET
def get_user_data(request):
	# get user id and tokens
	try:
		user_id = request.GET.get('socialUserID')
		access_token = request.GET.get('accessToken')
	except:
		return HttpResponseBadRequest()
	# Get user
	userinfo = get_user(user_id,access_token)
	if userinfo == None:
		return HttpResponseServerError()
	return_data = {}
	#new long access token
	#new user_type
	if userinfo.is_employee:
		user_type = "employee"

	elif userinfo.is_employer:
		user_type = "employer"
	return_data['userType'] = user_type
	return_data['accessToken'] = userinfo.access_token
	return_data['id'] = userinfo.id
	address= {}
	address['address'] = userinfo.address
	address['city'] = userinfo.city
	address['region'] = userinfo.region
	address['country'] = userinfo.country
	address['postalCode'] = userinfo.postal_code
	return_data['address']=  address
	return HttpResponse(json.dumps(return_data))

	#no employee or employer found, return none







#FUNCTION USED TO REGISTER USER AS AN EMPLOYEE
#input: {user_id,accesstoken}
@require_POST
@csrf_exempt
def choose_user_type(request):
	#get all data
	try:
		post_data = ast.literal_eval(request.body)
		print post_data
		social_user_id = post_data.get('socialUserID')
		access_token  = post_data.get('accessToken')
		user_type = post_data.get('userType')
	except:
		return HttpResponseBadRequest()

	#if there is no user, then we have to relog
	#try:
	user = get_user(social_user_id,access_token)
	if user== None:
	    return HttpResponseForbidden()
	try:
		if user_type =="employer":
			user.is_employer = True
			employer= EmployerInfo(userinfo = user)
			employer.save()
		elif user_type == "employee":
			user.is_employee = True
			employee= EmployeeInfo(userinfo=user,
						salary_low = 10,
						salary_high = 999,
						)
			employee.save()
			#create all job types
			job_types = JobType.objects.all()
			create_statement= []
			for job_type in job_types:
			    new_job_class = EmployeeJobType(userinfo = user,
							    job_type = job_type)
			    create_statement.append(new_job_class)
			EmployeeJobType.objects.bulk_create(create_statement)
		user.save()
	except Exception, e:
		print e
		return HttpResponseServerError()
	return HttpResponse()

#FUNCTION register some of user data in first step, (jobs interested in and salary)
#input: salaryHigh, salaryLow, selectedJobTypes
@require_POST
@csrf_exempt
def employee_first_step(request):
	try:
		post_data = ast.literal_eval(request.body)
		social_user_id = post_data.get('socialUserID')
		access_token  = post_data.get('accessToken')
		salary_low = post_data.get('salaryLow')
		salary_high = post_data.get('salaryHigh')
		selected_job_types = post_data.get('selectedJobTypes')
	except:
		return HttpResponseBadRequest()
	#get the userinfo
	userinfo = get_user(social_user_id,access_token)
	if userinfo == None:
		return HttpResponseServerError()
	#check to see if emplyee info works
	try:
		employee_info = EmployeeInfo.objects.get(userinfo = userinfo)
		return HttpResponseServerError()
	except:
		pass
	#create initial employee info
	employee_info = EmployeeInfo(userinfo = userinfo,
								salary_low = salary_low,
								salary_high =salary_high)
	employee_info.save()

	#create job type filters
	for item in selected_job_types:
		try:
			job_type = JobType.objects.get(id=item)
		except:
			pass
		employee_job_type = EmployeeJobType(userinfo = userinfo, job_type = job_type)
		employee_job_type.save()
	return HttpResponse()


	return HttpResponse()

#FUNCTION USED TO CHANGE PROFILE PICTURE OF USER
#input: social token, userid, pictureUrl
@require_POST
@csrf_exempt
def employer_first_step(request):
	try:
		print request.body
		post_data = ast.literal_eval(request.body)
		social_user_id = post_data.get('socialUserID')
		access_token  = post_data.get('accessToken')
		company_name = post_data.get('companyName')
		company_description = post_data.get('companyDescription')
	except:
		return HttpResponseBadRequest()
	userinfo = get_user(social_user_id,access_token)
	if userinfo == None:
		return HttpResponseServerError()
	#check to see if emplyee info works
	try:
		employer_info = EmployerInfo.objects.get(userinfo = userinfo)
		return HttpResponseServerError()
	except:
		pass
	#create initial employee info
	employer_info = EmployerInfo(userinfo = userinfo,
								name = company_name,
								description = company_description)
	employer_info.save()

	return HttpResponse()
#FUNCTION USED TO SET QUICKBLOX ID OF EMPLOYEE
@require_POST
@csrf_exempt
def set_quickblox_id(request):
    try:
	post_data = ast.literal_eval(request.body)
	social_user_id = post_data.get('socialUserID')
	access_token  = post_data.get('accessToken')
	quickblox_id = post_data.get('QBID')
    except:
	return HttpResponseBadRequest()

    user= get_user(social_user_id,access_token)
    if user== None:
	return HttpResponseServerError()
    print quickblox_id

    user.quickblox_id = quickblox_id
    user.save()
    return HttpResponse(quickblox_id)



#FUNCTION USED TO CHANGE PROFILE PICTURE OF USER
#input: social token, userid, pictureUrl
@require_POST
@csrf_exempt
def change_profile_picture(request):
	try:
		post_data = ast.literal_eval(request.body)
		social_user_id = post_data.get('socialUserID')
		access_token  = post_data.get('accessToken')
		new_profile_url = post_data.get('pictureUrl')
	except:
		return HttpResponseBadRequest()

	userinfo = get_user(social_user_id,access_token)
	if userinfo == None:
		return HttpResponseServerError()
	userinfo.profile_picture_url = new_profile_url
	#check to see where to redirect use
	try:
		EmployerInfo.objects.get(userinfo = userinfo)
		return HttpResponse('employer')
	except:
		return HttpResponse('employee')
#FUNCTION used to get employer profile data
@require_GET
def get_employer_profile_data(request):
    try:
	user_id = request.GET.get('id')
    except:
	return HttpResponseBadRequest()
    try:
	user_details = UserInfo.objects.get(id = user_id)
    except:
	return HttpResponseServerError()
    #get employer data
    try:
	employer_data  = EmployerInfo.objects.get(userinfo = user_details)
    except:
	return HttpResponseServerError()

    return_data =  {}
    return_data['id'] = user_details.id
    return_data['firstName'] = user_details.first_name
    return_data['profilePictureUrl'] = user_details.profile_picture_url
    return_data['lastLongitude'] = str(user_details.last_longitude)
    return_data['lastLatitude'] = str(user_details.last_latitude)
    return_data['description'] = employer_data.description
    return HttpResponse(json.dumps(return_data))



#FUNCTION used to get employee profile data
@require_GET
def get_employee_profile_data(request):
    try:
	user_id = request.GET.get('id')
    except:
	return HttpResponseBadRequest()
    try:
	employee = UserInfo.objects.get(id = user_id)
    except:
	return HttpResponseServerError()
    #get employer data
    try:
	employee_data  = EmployeeInfo.objects.get(userinfo = employee)
    except:
	return HttpResponseServerError()
    new_employee= {}
    new_employee['id'] = employee.id
    new_employee['amountMade'] = str(employee_data.amount_made)
    new_employee['firstName'] = employee.first_name
    new_employee['lastName'] = employee.last_name
    new_employee['profilePictureURL'] = employee.profile_picture_url
    new_employee['description'] = employee_data.description
    new_employee['lastLatitude'] = str(employee.last_latitude)
    new_employee['lastLongitude'] = str(employee.last_longitude)
    #get employee gender
    if employee.sex == "male":
	new_employee['gender'] = "m"
    else:
	 new_employee['gender'] = "f"
    #get employee skills
    try:
        employee_skills = EmployeeSkill.objects.filter(userinfo = employee).order_by('value')
    except:
         return HttpResponseServerError()
    #add employee skills to employee
    return_employee_skills = []
    for skill in employee_skills:
        return_employee_skills.append(skill.value)
    #add employee skills
    new_employee['employeeSkills'] = return_employee_skills
    #get employee education
    try:
        employee_education = EmployeeEducation.objects.filter(userinfo = employee).order_by('value')
    except:
        return HttpResponseServerError()
    return_employee_education = []
    for education in employee_education:
	new_employee_education = {}
	new_employee_education['value'] = education.value
	new_employee_education['institution'] = education.institution
        return_employee_education.append(new_employee_education)
    #add employee educaton to employee
    new_employee['employeeEducation'] = return_employee_education
    #get employee work experience
    try:
        employee_work_experience = EmployeeWorkExperience.objects.filter(userinfo = employee).order_by('title')
    except:
        return HttpResponseServerError()
    return_work_experience	= []
    #iterate through new work experience and make sure its addded
    for work_experience in employee_work_experience:
        new_work_experience={}
        new_work_experience['companyName'] = work_experience.company_name
        new_work_experience['title'] = work_experience.title
        #append work experience to new work experience
        return_work_experience.append(new_work_experience)
    #add to return data
    new_employee['employeeWorkExperience'] = return_work_experience
    #add new employee to set of emp
    #return the employees
    print new_employee
    return HttpResponse(json.dumps(new_employee))

#FUNCTION USED TO UPDATE ADDRESS  OF USER
@require_POST
@csrf_exempt
def update_address(request):
	try:
		post_data = ast.literal_eval(request.body)
		social_user_id = post_data.get('socialUserID')
		access_token  = post_data.get('accessToken')
		address_full= post_data.get('address')
		address= address_full.get('address')
		city = address_full.get('city')
		region = address_full.get('region')
		country = address_full.get('country')
		postal_code=address_full.get('postalCode', None)
	except:
		return HttpResponseBadRequest()
	userinfo = get_user(social_user_id,access_token)
	if userinfo == None:
		return HttpResponseServerError()
	#check to see if emplyee info works
	try:
	    userinfo.address = address
	    userinfo.city = city
	    userinfo.region = region
	    userinfo.country = country
	    if postal_code != None:
		userinfo.postal_code = postal_code
	    userinfo.save()
	except Exception,e:
	    print e
	    return HttpResponseServerError()
	return HttpResponse()

#FUNCTION USED TO UPDATE ADDRESS  OF USER
@require_POST
@csrf_exempt
def set_email(request):
	try:
		post_data = ast.literal_eval(request.body)
		print post_data
		social_user_id = post_data.get('userID')
		access_token  = post_data.get('accessToken')
		email = post_data.get('email')
	except:
		return HttpResponseBadRequest()
	userinfo = get_user(social_user_id,access_token)
	if userinfo == None:
		return HttpResponseServerError()
	#check to see if emplyee info works
	try:
	    print email
	    userinfo.email = email
	    userinfo.save()
	except:
	    return HttpResponseServerError()
	return HttpResponse()
#EDIT EMPLOYER PROFILE INFO
@csrf_exempt
def edit_employer_data(request):
    if request.method == "GET":
	try:
	    user_id = request.GET.get('userID')
	    access_token = request.GET.get('accessToken')
	except:
	    return HttpResponseBadRequest()
	user = get_user(user_id, access_token)
	if user == None:
	    return HttpResponseForbidden()
	try:
	    employer_data =  EmployerInfo.objects.get(userinfo = user)
	except:
	    return HttpResponseForbidden
	return_data = {}
	return_data['profilePictureURL'] = user.profile_picture_url
	return_data['description'] = employer_data.description
	#return the data
	return HttpResponse(json.dumps(return_data))

    elif request.method == "POST":
	try:
	    post_data = ast.literal_eval(request.body)
	    user_id = post_data.get('userID')
	    access_token  = post_data.get('accessToken')
	    description = post_data.get('description')
	except:
	    return HttpResponseBadRequest()

	userinfo = get_user(user_id,access_token)
	if userinfo == None:
		return HttpResponseServerError()

	try:
	    employer_info = EmployerInfo.objects.get(userinfo = userinfo)
	except Exception, e:
	    print e
	    return HttpResponseForbidden()

	employer_info.description = description
	employer_info.save()
	return HttpResponse()



@csrf_exempt
@require_GET
#get edit data for user
def edit_employee_data(request):
    try:
	user_id =  request.GET.get('userID')
	access_token = request.GET.get('accessToken')
    except:
	return HttpResponseBadRequest()
    #get user
    employee = get_user(user_id, access_token)
    if employee == None:
	return HttpResponseForbidden()
    try:
	employee_data= EmployeeInfo.objects.get(userinfo= employee)
    except Exception,e:
	print e
	return HttpResponseForbidden()

    new_employee= {}
    new_employee['id'] = employee.id
    new_employee['firstName'] = employee.first_name
    new_employee['lastName'] = employee.last_name
    new_employee['profilePictureURL'] = employee.profile_picture_url
    new_employee['description'] = employee_data.description
    try:
        employee_skills = EmployeeSkill.objects.filter(userinfo = employee).order_by('value')
    except:
         return HttpResponseServerError()
    #add employee skills to employee
    return_employee_skills = []
    for skill in employee_skills:
	employee_skill = {}
	employee_skill['id']  = skill.id
	employee_skill['value'] = skill.value
        return_employee_skills.append(employee_skill)
    #add employee skills
    new_employee['employeeSkills'] = return_employee_skills
    #get employee education
    try:
        employee_education = EmployeeEducation.objects.filter(userinfo = employee).order_by('value')
    except:
        return HttpResponseServerError()
    return_employee_education = []
    for employee_education in employee_education:
	education = {}
	education['value'] = employee_education.value
	education['id'] = employee_education.id
	education['institution'] = employee_education.institution
        return_employee_education.append(education)
    #add employee educaton to employee
    new_employee['employeeEducation'] = return_employee_education
    #get employee work experience
    try:
        employee_work_experience = EmployeeWorkExperience.objects.filter(userinfo = employee).order_by('title')
    except:
        return HttpResponseServerError()
    return_work_experience	= []
    #iterate through new work experience and make sure its addded
    for work_experience in employee_work_experience:
        new_work_experience={}
        new_work_experience['companyName'] = work_experience.company_name
	new_work_experience['id'] = work_experience.id
        new_work_experience['title'] = work_experience.title
        #append work experience to new work experience
        return_work_experience.append(new_work_experience)
    #add to return data
    new_employee['employeeWorkExperience'] = return_work_experience
    #add new employee to set of emp
    #return the employees
    return HttpResponse(json.dumps(new_employee))

@csrf_exempt
@require_POST
def update_employee_data(request):
    try:
	post_data = ast.literal_eval(request.body)
	user_id = post_data.get('userID')
	access_token  = post_data.get('accessToken')
	type_category = post_data.get('typeCategory')
	data = post_data.get('data')
    except:
        return HttpResponseBadRequest()
    userinfo = get_user(user_id,access_token)
    if userinfo == None:
	    return HttpResponseServerError()
    #return data
    return_data = {}
    print type_category
    if type_category == "skill":
	#check to see if skill is already added
	print data
	try:
	    employee_skill = EmployeeSkill.objects.get(userinfo=userinfo, value=data)
	    error_message = "Skill %s already exists" % (data)
	    return HttpResponseServerError(error_message)
	except:
	    pass
	employee_skill = EmployeeSkill(userinfo = userinfo,value = data)
	employee_skill.save()
	return_data['id'] = employee_skill.id
	return_data['value'] = employee_skill.value
    elif type_category == "description":
	try:
	    employee_info = EmployeeInfo.objects.get(userinfo = userinfo)
	    employee_info.description = data
	    employee_info.save()
	    return_data=employee_info.description
	except Exception, e:
	    print e
	    return HttpResponseServerError()

    elif type_category == "work experience":
	#check to see if work experience exists
	try:
	    work_experience = EmployeeWorkExperience.objects.get(userinfo=userinfo,
								company_name= data.get('company'),
								title = data.get('title'))
	    error_message = "You cannot add the same work experience twice"
	    return HttpResponseServerError(error_message)
	except:
	    pass
	employee_work_experience = EmployeeWorkExperience(userinfo = userinfo,
							company_name = data.get('company'),
							title = data.get('title'))
	employee_work_experience.save()
	return_data['id'] = employee_work_experience.id
	return_data['title'] =employee_work_experience.title
	return_data['companyName'] = employee_work_experience.company_name
    elif type_category == "education":
    #check to see if user has same education saved
	try:
	    education = EmployeeEducation.objects.get(userinfo = userinfo,
						 value = data.get('value'),
						 institution = data.get('institution')
		)
	    error_message = "You cannot add the same education twice"
	    return HttpResponseServerError(error_message)
	except:
	    pass
	#create employee_education
	try:
	    employee_education = EmployeeEducation(userinfo=userinfo,
						value = data.get('value'),
						institution = data.get('institution'))
	    employee_education.save()
	except:
	    pass

	return_data['id'] = employee_education.id
	return_data['value'] = employee_education.value
	return_data['institution'] = employee_education.institution
   #return data
    return HttpResponse(json.dumps(return_data))

#delete employee skill
@csrf_exempt
@require_POST
def delete_employee_data(request):
    try:
	post_data = ast.literal_eval(request.body)
	user_id = post_data.get('userID')
	access_token  = post_data.get('accessToken')
	type_category = post_data.get('typeCategory')
	delete_id = post_data.get('delete_id')
    except:
        return HttpResponseBadRequest()
    userinfo = get_user(user_id,access_token)
    if userinfo == None:
	    return HttpResponseServerError()
    #return data
    return_data = {}
    if type_category == "skill":
#get skill to
	try:
	    employee_skill = EmployeeSkill.objects.get(userinfo = userinfo, id = delete_id)
	    employee_skill.delete()
	except:
	    return HttpResponseServerError()

    elif type_category == "work experience":
	 #get work experience to dlete
	try:
	    employee_work_experience = EmployeeWorkExperience.objects.get(userinfo= userinfo , id = delete_id)
	    employee_work_experience.delete()
	except:
	    return HttpResponseServerError()
    elif type_category == "education":
	#get education to delete
	try:
	    employee_education = EmployeeEducation.objects.get(userinfo = userinfo, id = delete_id)
	    employee_education.delete()
	except:
	    return HttpResponseServerError()


    return HttpResponse()
#get discovery preferences of the user
@require_GET
def get_user_discovery_preferences(request):
    try:
	user_id = request.GET.get('userID')
	access_token = request.GET.get('accessToken')
    except:
	return HttpResponseBadRequest()
    user = get_user(user_id, access_token)
    #get user
    if user == None:
	return HttpResponseForbidden()
    #get employee data
    try:
	employee_data = EmployeeInfo.objects.get(userinfo = user)
    except Exception, e:
	print e
	return HttpResponseServerError()
    #get employee job types
    try:
	employee_job_types = EmployeeJobType.objects.filter(userinfo = user)
    except Exception,e:
	print e
	return HttpResponseServerError()
    discovery_preferences = {}
    discovery_preferences['lowestAcceptableWage'] = employee_data.salary_low
    discovery_preferences['searchDistance'] = employee_data.distance_filter
    job_types = []
    for job_type in employee_job_types:
	job_types.append(job_type.job_type.id)
    discovery_preferences['selectedJobTypes'] = job_types
    return HttpResponse(json.dumps(discovery_preferences))
#FUNCTION get address of useer when they want to add using address




@require_POST
@csrf_exempt
def update_user_discovery_preferences(request):
    try:
	post_data= ast.literal_eval(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	search_distane = post_data.get('searchDistance')
	lowest_acceptable_wage = post_data.get('lowestAcceptableWage')
	selected_job_types = post_data.get('selectedJobTypes')
    except :
	return HttpResponseBadRequest()

    #get user
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseServerError

#get user data
    try:
	employee_data = EmployeeInfo.objects.get(userinfo = user)
	employee_data.distance_filter = search_distane
	employee_data.salary_low = lowest_acceptable_wage
	employee_data.save()
    except Exception, e:
	print e
	return HttpResponseServerError()

    #get user job types
    try:
	job_types = EmployeeJobType.objects.filter(userinfo =user)
	for job_type in job_types:
	    is_selected  = False
	    for  job in selected_job_types:
		if job_type.job_type.id == job:
		    print job
		    is_selected = True

	    #if job_type isnt in selected, delete it
	    if is_selected == False:
		job_type.delete()

	for job in selected_job_types:
	    add_job = True
	    for job_type in job_types:
		#if job type is added
		if job_type.job_type.id == job:
		    add_job = False
	    if add_job == True:
		new_job_type = JobType.objects.get(id = job)
		employee_job_type = EmployeeJobType(userinfo = user, job_type = new_job_type)
		employee_job_type.save()
    except Exception,e:
	print e
	return HttpResponseServerError()
    return HttpResponse()






#update location of user
@require_POST
@csrf_exempt
def update_location(request):
    try:
	post_data= ast.literal_eval(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	current_latitude = post_data.get('currentLatitude')
	current_longitude = post_data.get('currentLongitude')
    except :
	return HttpResponseBadRequest()

    #get user
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseServerError

    user.last_latitude = current_latitude
    user.last_longitude = current_longitude
    user.save()
    return HttpResponse()
#FUNCTION:check to see if there are any new notifications
@require_GET
@csrf_exempt
def get_new_notifications(request):
    try:
	user_id = request.GET.get('userID')
	access_token = request.GET.get('accessToken')
	notification_type = request.GET.get('notificationType')
	user_type = request.GET.get('userType')

    except:
	return HttpResponseBadRequest()

    user = get_user(user_id,access_token)
    if user == None:
	return HttpResponseForbidden()
    if notification_type == "matches":
	if user_type == "employee":
	   return HttpResponse(json.dumps(user.has_new_match_employee))
	elif user_type == "employer":
	   return HttpResponse(json.dumps(user.has_new_match_employer))
    elif notification_type =="messages":
	if user_type == "employee":
	    return HttpResponse(json.dumps(user.has_new_message_employee))
	if user_type == "employer":
	    return HttpResponse(json.dumps(user.has_new_message_employer))
#update new notifications of user
@require_POST
@csrf_exempt
def update_new_notifications(request):
    try:
	post_data= ast.literal_eval(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	notification_type = post_data.get('notificationType')
	user_type = post_data.get('userType')
	value = post_data.get('value')
	print request.body
	print post_data
    except Exception,e :
	print e
	return HttpResponseBadRequest()
    if value == "true":
	value = True
    else :
	value = False

    #get user
    user = get_user(user_id, access_token)
    if user == None:
	return HttpResponseBadRequest()
    if notification_type == "matches":
	if user_type == "employer":
	    user.has_new_match_employer = value
	    user.save()
	    return HttpResponse(json.dumps(user.has_new_message_employer))
	elif user_type == "employee":
	    user.has_new_match_employee = value
	    user.save()
	    return HttpResponse(json.dumps(user.has_new_message_employee))
    elif notification_type == "messages":
	print "ASDASD ==== ASDASDDS"
	if user_type == "employee":
	    user.has_new_message_employee = value
	    user.save()
	    return HttpResponse(json.dumps(user.has_new_message_employee))
	elif user_type == "employer":
	    user.has_new_message_employer = value
	    user.save()
	    return HttpResponse(json.dumps(user.has_new_message_employer))

#request Cheque
@require_POST
@csrf_exempt
def request_cheque(request):
    try:
	post_data= ast.literal_eval(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
    except Exception, e:
	print e
	return HttpResponseBadRequest()
    user =  get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    try:
	employee_info  = EmployeeInfo.objects.get(userinfo = user)
    except Exception,e:
	print e
	return HttpResponseServerError()

#create cheque request
    try:
	cheque_request = EmployeeChequeRequest(employee = user, amount_made = employee_info.amount_made)
	cheque_request.save()
#set made to 0
	employee_info.amount_made = 0
	employee_info.save()
    except Exception,e:
	print e
	return HttpResponseServerError()
    return HttpResponse()
#check to see if a usertype exists
@csrf_exempt
def has_user_type(request):
    try:
	user_id = request.GET.get('userID')
	access_token = request.GET.get('accessToken')
	user_type = request.GET.get('userType')
    except:
	return HttpResponseBadRequest()
    user  = get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    #check to see if they have one that exists
    if user_type == "employee":
	try:
	    employer = EmployerInfo.objects.get(userinfo = user)
	    return HttpResponse('employer exists')
	except Exception,e:
	    print e
	    return HttpResponseServerError()
    elif user_type == "employer":
	try:
	    employee = EmployeeInfo.objects.get(userinfo = user)
	    return HttpResponse('employer exists')
	except Exception,e:
	    print e
	    return HttpResponseServerError()
    else:
	return HttpResponseServerError()


#crop picture using base 64 encoded images
@require_POST
@csrf_exempt
def crop_picture(request):
    try:
	post_data= ast.literal_eval(request.body)
	user_id = post_data.get('userID')
	access_token = post_data.get('accessToken')
	image = post_data.get('image')
    except Exception, e:
	print e
	return HttpResponseBadRequest()
    user =  get_user(user_id, access_token)
    if user == None:
	return HttpResponseForbidden()
    #connect to s3
#change dataurl to image
    image = image.split(',')[1]
    img =  cStringIO.StringIO(image.decode('base64'))
    im = Image.open(img)
#save image to cstringio to avoid writing to disk
    out_im2 = cStringIO.StringIO()
    im.save(out_im2,'PNG')
    conn = S3Connection('KIAJ5XSVQ6KCGN3B3VA', 'XJSekHmtCJPUzfyHyYx5+EDz0yaftzeNAtA6Epbt')
    conn = S3Connection()
    import boto
    conn = boto.connect_s3()
    b = conn.get_bucket('jobswipe') # substitute your bucket name here
    from boto.s3.key import Key
    k = Key(b)
    k.key = str(user.social_user_id) +"_profile_picture"
    k.set_contents_from_string(out_im2.getvalue())
    k.set_acl('public-read')
    url = k.generate_url(expires_in=0, query_auth=False)
    from random import randint
    rand_int = str(randint(0,300))
    user.profile_picture_url = url+"?"+rand_int
    user.save()
    return HttpResponse(url)

@csrf_exempt
def user_login(request):
	#check to see if user is already created.. if they are, redirect to dashboard

	if request.method == "GET":
		return render_to_response("login.html", context_instance=RequestContext(request))
	if request.method == "POST":
		username = request.POST.get('username')
		password= request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user is not None:
			login(request, user)
			return redirect('/addscrapedjobs')
		else:
			return HttpResponse('wrong credentials')

def get_csrf_token(request):
	csrf_token = django.middleware.csrf.get_token(request)

#HELPER FUNCTION
#get the long token by giving in short live token
def get_facebook_long_token(short_lived_token):
	app_id = 797804093619145
	app_secret = "857f753e0f92c28662da8a99d97c24e2"
	url = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=%s&client_secret=%s&fb_exchange_token=%s" %(app_id, app_secret,short_lived_token )
	try:
		returned_token = urllib2.urlopen(url).read()
		returned_token = returned_token.split('=')[1]
		returned_token = returned_token.split('&')[0]
		return returned_token
	except Exception,e:
		print e
		return HttpResponseBadRequest()
#helper function to get user, if it returns none, then there is no user
def get_user(user_id,auth_token):
	try:
		user = UserInfo.objects.get(social_user_id = user_id , access_token = auth_token)
		return user
	except:
		return None

def get_quickblox_session(request):
	appId = '18291'
	authKey = '92SqnrmCT--GMb6'
	authSecret = 'KbJ9mkBPu-r8TaM'

	import time, random, hmac, urllib, httplib
	from hashlib import sha1

	nonce = str(random.randint(1, 10000))
	timestamp = str(int(time.time()))

	signature_raw_body = ("application_id=" + appId + "&auth_key=" + authKey +
	            "&nonce=" + nonce + "&timestamp=" + timestamp)

	signature = hmac.new(authSecret, signature_raw_body, sha1).hexdigest()

	params = {'application_id': appId,
	                           'auth_key': authKey,
	                           'timestamp': timestamp, 'nonce' : nonce,
	                           'signature' : signature}

	jsonData = json.dumps(params)

	httpHeaders = {'Content-Type': 'application/json',
                   'QuickBlox-REST-API-Version': '0.1.0'}

	r = requests.post('https://api.quickblox.com/session.json', data=jsonData, headers = httpHeaders)
	print 'status code:', r.status_code
	responseJson = r.text
	print responseJson
	response = json.loads(responseJson)
	print type(response)
	print response
	return HttpResponse(json.dumps(response))

def decode_base64(data):
    data = data.split(',')[1]
    missing_padding = 4 - len(data) % 4
    if missing_padding:
	data += b'='* missing_padding
	return base64.decodestring(data)

